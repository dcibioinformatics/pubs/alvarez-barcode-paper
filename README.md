# Reads alignment, count analysis and figure generating scripts for manuscript

## Code/Align
Include all the scripts for conducting reproduciable alignment of reads to the library sequence

To run the reads count for barcode:
1) create an empty directory.
2) zcat all fastq files for each sample into a new file in the empty directory
3) cd into the directory and run
```
./barcode-pipeline ref filt
```
where ref is the reference fasta and filt is the maximum allowed number of mismatches

## Code/Analysis
Include all the scripts for conducting reproduciable statistical analysis on the count data

To run the analysis, cd to the folder and
```
make
```

## Code/GATK
Instructions and YAML files needed to run GATK pre-processing of fastq files on AWS EKS

## Code/PlotScripts
Include all the scripts for generating all the figures shown in the manuscripts

To run the code, code to the folder and
```
R CMD BATCH Figure_2.R
R CMD BATCH Figure_3.R
R CMD BATCH Figure_4.R
R CMD BATCH Figure_5.R
R CMD BATCH Figure_99142.R
R CMD BATCH Figure_SI.R
```
## Code/Codex2
Include scripts for running codex2 on all sample bam files given the capture file:
* `CODEX_qc.R` is used to calculate gc content, mappability and coverage from bam files.
* `runCODEX.R` is used to run segmentation parallely across all autosomes.
* `cnv_plot.R` is used to generate Copy Number Variation plots of all autosomes, chromosome 6 and Met gene regions.

To run all the code, cd to the folder and
```
R CMD BATCH CODEX_qc.R
R CMD BATCH runCODEX.R
R CMD BATCH cnv_plot.R
```

Caveat: The mappability calculation in CODEX_qc.R will take long time (10 days), a pre-calculated R object is stored on ccis4301:/data2/workspace/DCI/Alvarez/Code/reference.rds

## Reports
Include pdf reports for the analysis and figure generation as reference

## Revision
Include all the codes and resulted figure and report for the resubmit

