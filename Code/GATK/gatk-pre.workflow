---
apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: gatk-pre-
spec:
  entrypoint: gatk-pre

  arguments:
    parameters:
    - name: sample-list
      value: |
        [{"uid": "test", "R1": "test_R1.fastq.gz", "R2": "test_R2.fastq.gz"}]
    - name: bucket
      value: bucket
    - name: endpoint
      value: s3.amazonaws.com
    - name: secretKeyName
      value: s3-key
    - name: accesskey
      value: accesskey
    - name: secretkey
      value: secretkey
    - name: workdir
      value: workdir
    - name: genome
      value: genome
    - name: genome-dir
      value: genome
    - name: reference-dir
      value: reference
    - name: threads
      value: 6
    - name: dbsnp
      value: mgp.v4.snps.dbSNP.vcf.gz
    - name: indel
      value: mgp.v4.indels.dbSNP.vcf.gz
    - name: capture
      value: capture.bed


  templates:
  - name: gatk-pre
    steps:
    - - name: gatk-pre-base
        template: gatk-pre-steps
        arguments:
          parameters:
          - name: uid
            value: "{{item.uid}}"
          - name: R1
            value: "{{item.R1}}"
          - name: R2
            value: "{{item.R2}}"
        withParam: "{{workflow.parameters.sample-list}}"


  - name: gatk-pre-steps
    inputs:
      parameters:
        - name: uid
        - name: R1
        - name: R2

    steps:
    - - name: bwa # add to this - artifact S3 repo, input/output in S3 repo
        template: bwa-template
        arguments:
          parameters:
          - name: uid
            value: "{{inputs.parameters.uid}}"
          - name: R1
            value: "{{inputs.parameters.R1}}"
          - name: R2
            value: "{{inputs.parameters.R2}}"
          - name: threads
            value: "{{workflow.parameters.threads}}"


    - - name: picard-readgroups  # now sort by query-name, next step sorts by position after but works better/faster with queryname
        template: picard-readgroups-template
        arguments:
          parameters:
          - name: uid
            value: "{{inputs.parameters.uid}}"
          - name: RGID
            value: "{{steps.bwa.outputs.parameters.RGID}}"


    - - name: MarkDuplicatesSpark
        template: MarkDuplicatesSpark-template
        arguments:
          parameters:
          - name: uid
            value: "{{inputs.parameters.uid}}"
          - name: threads
            value: "{{workflow.parameters.threads}}"


    - - name: BQSR
        template: BQSR-template
        arguments:
          parameters:
          - name: uid
            value: "{{inputs.parameters.uid}}"


    - - name: picard-metrics
        template: picard-metrics-template
        arguments:
          parameters:
          - name: uid
            value: "{{inputs.parameters.uid}}"


  - name: bwa-template
    inputs:
      parameters:
      - name: uid
      - name: R1
      - name: R2
      - name: threads

      artifacts:
      - name: R1
        path: /data/R1.fastq.gz
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{inputs.parameters.uid}}/{{inputs.parameters.R1}}"

      - name: R2
        path: /data/R2.fastq.gz
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{inputs.parameters.uid}}/{{inputs.parameters.R2}}"

      - name: genome-bucket
        path: /data/genome
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.genome-dir}}.tgz"

    outputs:
      artifacts:
      - name: bucket-out
        path: /data/aligned
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/aligned.tgz"

      parameters:
      - name: RGID
        valueFrom:
          path: /data/RGID.txt

    script:
      image: dcibioinformatics/bwa
      command: [bash]
      source: |
        mkdir -p /data/aligned
        bwa mem -aM -t {{inputs.parameters.threads}} /data/genome/{{workflow.parameters.genome}} /data/R1.fastq.gz /data/R2.fastq.gz > /data/aligned/aligned.sam
        zcat /data/R1.fastq.gz | head -n 1 | awk '{match($0, /^@\W*([a-zA-Z0-9_:]+)/, m); split(m[1], n, ":"); print n[3]"."n[4]}' > /data/RGID.txt

      volumeMounts:
      - mountPath: /data
        name: data

      resources:
        requests:
          ephemeral-storage: 40Gi
        limits:
          ephemeral-storage: 165Gi
          memory: 40Gi
          cpu: 6


  - name: picard-readgroups-template
    inputs:
      parameters:
      - name: uid
      - name: RGID

      artifacts:
      - name: aligned
        path: /data/aligned
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/aligned.tgz"

    outputs:
      artifacts:
      - name: sorted
        path: /data/sorted
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/sorted.tgz"

    script:
      image: broadinstitute/picard
      command: [bash]
      source: |
        mkdir -p /data/sorted
        java -jar /usr/picard/picard.jar AddOrReplaceReadGroups I=/data/aligned/aligned.sam O=/data/sorted/sorted.bam RGID={{inputs.parameters.RGID}} RGPL=Illumina RGSM={{inputs.parameters.uid}} RGPU={{inputs.parameters.RGID}}.{{inputs.parameters.uid}} RGLB={{inputs.parameters.uid}} VALIDATION_STRINGENCY=STRICT SO=queryname CREATE_INDEX=true

      volumeMounts:
      - mountPath: /data
        name: data

      resources:
        requests:
          ephemeral-storage: 40Gi
        limits:
          ephemeral-storage: 165Gi
          memory: 40Gi
          cpu: 6


  - name: MarkDuplicatesSpark-template
    inputs:
      parameters:
      - name: uid
      - name: threads

      artifacts:
      - name: bucket-in
        path: /data/sorted
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/sorted.tgz"


    outputs:
      artifacts:
      - name: marked
        path: /data/marked
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/marked.tgz"

      - name: metrics
        path: /data/dup_metrics.txt
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/dup_metrics.txt"

    script:
      image: broadinstitute/gatk:4.1.3.0
      command: [bash]
      source: |
        mkdir -p /data/marked
        gatk MarkDuplicatesSpark -I /data/sorted/sorted.bam -O /data/marked/marked.bam -M /data/dup_metrics.txt -OBI --conf "'spark.executors.cores={{inputs.parameters.threads}}'"

      volumeMounts:
      - mountPath: /data
        name: data

      resources:
        requests:
          ephemeral-storage: 40Gi
        limits:
          ephemeral-storage: 165Gi
          memory: 40Gi
          cpu: 6

  - name: BQSR-template
    inputs:
      parameters:
      - name: uid

      artifacts:
      - name: bucket-in
        path: /data/marked
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/marked.tgz"

      - name: genome-bucket
        path: /data/genome
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.genome-dir}}.tgz"

      - name: reference-bucket
        path: /data/reference
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.reference-dir}}.tgz"

    outputs:
      artifacts:
      - name: bucket-out
        path: /data/recal
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/recal.tgz"

    script:
      image: broadinstitute/gatk:4.1.3.0
      command: [bash]
      source: |
        mkdir -p /data/recal
        gatk BaseRecalibrator -I /data/marked/marked.bam --known-sites "/data/reference/{{workflow.parameters.dbsnp}}" --known-sites "/data/reference/{{workflow.parameters.indel}}" -O /data/recal/recal_table.txt -R "/data/genome/{{workflow.parameters.genome}}" -L "/data/reference/{{workflow.parameters.capture}}"
        gatk ApplyBQSR -I /data/marked/marked.bam -O /data/recal/recal.bam -R "/data/genome/{{workflow.parameters.genome}}" -bqsr /data/recal/recal_table.txt -OBI

      volumeMounts:
      - mountPath: /data
        name: data

      resources:
        requests:
          ephemeral-storage: 40Gi
        limits:
          ephemeral-storage: 165Gi
          memory: 40Gi
          cpu: 6


  - name: picard-metrics-template
    inputs:
      parameters:
      - name: uid

      artifacts:
      - name: recal
        path: /data/recal
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/recal.tgz"


      - name: genome-bucket
        path: /data/genome
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.genome-dir}}.tgz"


      - name: reference-bucket
        path: /data/reference
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.reference-dir}}.tgz"



    outputs:
      artifacts:
      - name: metrics
        path: /data/metrics.txt
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/metrics.txt"

    script:
      image: broadinstitute/picard
      command: [bash]
      source: |
        java -jar /usr/picard/picard.jar CollectWgsMetrics R=/data/genome/{{workflow.parameters.genome}} INTERVALS=/data/reference/{{workflow.parameters.intervals}} I=/data/recal/recal.bam O=/data/metrics.txt

      volumeMounts:
      - mountPath: /data
        name: data

      resources:
        requests:
          ephemeral-storage: 40Gi
        limits:
          ephemeral-storage: 165Gi
          memory: 40Gi
          cpu: 6


  volumes:
  - name: data
    emptyDir: {}
