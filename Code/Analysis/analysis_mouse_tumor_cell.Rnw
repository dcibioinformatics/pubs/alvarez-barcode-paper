\documentclass[10pt,xcolor=x11names,compress]{beamer}

\usepackage{lmodern}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{booktabs}
\usepackage[round]{natbib}
\renewcommand{\newblock}{} % Get natbib and beamer working together: http://tex.stackexchange.com/questions/2569/beamer-and-natbib
\usetheme{Copenhagen}
%\VignetteEngine{knitr::knitr}
%\VignetteIndexEntry{fastJT}

\newcommand{\R}{\texttt{R}}
\newcommand{\pkgname}{\texttt{fastJT}}
\newcommand{\Rcpp}{\texttt{Rcpp}}
\newcommand{\openmp}{\texttt{OpenMP}}

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]

\setbeamertemplate{enumerate item}{\insertenumlabel}
\setbeamertemplate{enumerate subitem}{\alph{enumii}.}


\begin{document}
%\SweaveOpts{concordance=TRUE}
<<setup1, echo=FALSE>>=
require(knitr)
@

<<setup2, echo=FALSE>>=
options(width=80)  # make the printing fit on the page
set.seed(1121)     # make the results repeatable
stdt<-date()
library(RColorBrewer)
library(reshape2)
library(stringr)
suppressMessages(library(dplyr, warn.conflicts = FALSE, quietly=TRUE))
suppressMessages(library(plyr, warn.conflicts = FALSE, quietly=TRUE))
@

%------------------------------------------------------------
%slide1: Project name, author,
%------------------------------------------------------------
\begin{frame}
\title{Reproduce Alvarez Results}
%\author{J.~Lin\inst{2} \and Y.~Wu\inst{2} \and X. ~Wang \inst{2} \and K. ~Owzar\inst{1,2}}
%\institute[] 
%{ 
%  \inst{1} Department of Biostatistics and Bioinformatics\\ Duke University Medical Center \and
%  \inst{2} DCI Bioinformatics Shared Resources\\ Duke University Medical Center
%}
%\date{08/29/2017}
\titlepage
\end{frame}


\scriptsize
\section{Pie Chart}
\begin{frame}[fragile]{Set up}
Read in files and setups
<<setup>>=
# load 2016 data
wd <- '/home/jl354/Works/05_Alvarez/alvarez_mouse_cellecta_crispr_sreen/alvarez-paper/'
countFile_2016 <- file.path(wd,"./Data/20161014_HiSeq-42of42-with_repeats-revised_counts.csv")
countData_2016 <- read.csv(countFile_2016, header = TRUE)
sampleNames_2016 <- colnames(countData_2016)
sampleNames_2016 <- sampleNames_2016[1:length(sampleNames_2016)]
sampleNames_2016 <- gsub("X","", sampleNames_2016)
colnames(countData_2016) <- sampleNames_2016
conditionFile_2016 <- file.path(wd, "./Data/Alvarez_Cohort.csv")
conditionIndex_2016 <-  read.csv(conditionFile_2016, header=TRUE)
# load 2017 data
countFile <- file.path(wd, "./Data/all-counts_2017.tsv")
countData <- read.csv(countFile, header = TRUE)
@
\end{frame}
\begin{frame}[fragile]{Set up}
Read in files and setups
<<setupcont>>=
# change number of the sample to the same format in the cohort
sampleNames <- colnames(countData)
for(i in seq_len(length(sampleNames)))
  sampleNames[i] <- strsplit(sampleNames[i],"_")[[1]][1] 
sampleNames <- gsub("X","", sampleNames)
sampleNames <- str_replace_all(sampleNames, "[.]", "-")
# a wired naming style for 1668 which should be 1668_PAR
sampleNames[2] <- "1668"
colnames(countData) <- sampleNames
# change sample names to data
conditionFile <- file.path(wd, "./Data/Alvarez_Cohort_2017_add_to2016.csv")
conditionIndex <-  mutate_all(read.csv(conditionFile, header=TRUE, stringsAsFactors=FALSE,),.funs=toupper)
countData_2017_add <- countData[,as.character(c("barcode", conditionIndex[,1]))]
# merging cohort information
conditionIndex <- rbind(conditionIndex_2016[,c(1,2)], conditionIndex)
condition <- as.character(levels(conditionIndex[, 2]))
conditionIndex[,1] <- as.character(conditionIndex[,1])
conditionIndex[,2] <- as.character(conditionIndex[,2])
# merge count data
countData <- join(countData_2016, countData_2017_add, by ="barcode", type="full")
countData[is.na(countData)] <- 0
minCopies <- 1
#countData <- countData[which(countData[,2]!=0),]
@
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pie Chart Plot for James Results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Pie Chart for Barcode Counts Ratio in Each Sample}
Function to produce Pie Chart:
<<PieChart>>=
PieChart <- function(condition, conditionIndex, countData)
{
x <- countData[,c(conditionIndex[which(conditionIndex[,2] == condition),]$X)]
nSam <- ncol(x)

par(mfrow = c(2,ceiling(nSam/2)))
par(mgp=c(3,3,3),  mar=c(0,0,1,0))

for(i in 1:nSam){
  count <- x[which(x[,i]> minCopies),i]
  n <- length(count)
  qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
  col_vector = unlist(mapply(brewer.pal, qual_col_pals$maxcolors, 
                      rownames(qual_col_pals)))
  pie(count, labels=NA, col=col_vector, 
      main = colnames(x)[i], border = FALSE, cex.main=2)
}
}
@
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Pie Chart for Barcode Counts Ratio in Each Sample}
Library
\begin{figure} \begin{center}
<<pie1,echo=FALSE, out.width='2.8in'>>=
x <- countData[,c(conditionIndex[which(conditionIndex[,2] == "Library"),]$X)]
par(mgp=c(3,3,3),  mar=c(0,0,1,0))
count <- x[which(x> minCopies)]
n <- length(count)
qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
col_vector = unlist(mapply(brewer.pal, qual_col_pals$maxcolors, 
                    rownames(qual_col_pals)))
pie(count, labels=NA, col=col_vector, 
    main = "Library", border = FALSE, cex.main=2)
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Pie Chart for Barcode Counts Ratio in Each Sample}
Primary
\begin{figure} \begin{center}
<<pie3,echo=FALSE, out.width='2.8in'>>=
PieChart("Pri", conditionIndex,countData)
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Pie Chart for Barcode Counts Ratio in Each Sample}
Late Primary
\begin{figure} \begin{center}
<<pie2,echo=FALSE, out.width='2.8in'>>=
PieChart("LPri", conditionIndex,countData)
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Pie Chart for Barcode Counts Ratio in Each Sample}
Early Residule
\begin{figure} \begin{center}
<<pie4,echo=FALSE, out.width='2.8in'>>=
PieChart("earRes", conditionIndex,countData)
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Pie Chart for Barcode Counts Ratio in Each Sample}
Late Residule
\begin{figure} \begin{center}
<<pie5,echo=FALSE, out.width='2.8in'>>=
PieChart("LRes", conditionIndex,countData)
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Pie Chart for Barcode Counts Ratio in Each Sample}
Non Rcur
\begin{figure} \begin{center}
<<pie6,echo=FALSE, out.width='2.8in'>>=
PieChart("NRcur", conditionIndex,countData)
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Pie Chart for Barcode Counts Ratio in Each Sample}
Rcur
\begin{figure} \begin{center}
<<pie7,echo=FALSE, out.width='2.8in'>>=
PieChart("Rcur", conditionIndex,countData)
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Pie Chart for Barcode Counts Ratio in Each Sample}
Rcur cells
\begin{figure} \begin{center}
<<pie8,echo=FALSE, out.width='2.8in'>>=
PieChart("RCUR CELLS", conditionIndex,countData)
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Shannon Index
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Shannon Index}
\begin{frame}[fragile]{R Function to Compute Shannon Index}
\scriptsize
<<defineFunction, echo=TRUE>>=
ShannonIndex <- function(x,minCopies)
{
	x <- x[which(x > minCopies)]
	x <- x/sum(x)
	ShaInd <- x*log(x)
	-sum(ShaInd)
}
@
<<defineFunctio3n, echo=TRUE, eval=FALSE>>=
sIndex <- NULL
countDataTmp <- countData[,c("barcode",conditionIndex$X)]

for(i in 2:ncol(countDataTmp))
  sIndex[i-1] <- ShannonIndex(countDataTmp[,i], minCopies)

barplot(sIndex, names.arg = c(as.character(conditionIndex$Group)), 
        col= c(as.factor(conditionIndex$Group)), ylab = "Shannon Index", 
        cex.axis=1.5, cex.names = 1, cex.lab=1.5)
@
\end{frame}


\begin{frame}[fragile]{Shannon Index for Read Complexity}
\scriptsize
<<ComIndex_p_0>>=
minCopies = 1
@
\begin{figure}\begin{center}
<<ComIndex, echo = FALSE,out.width='4in', fig.width=9, fig.height=4>>=
sIndex <- NULL
countDataTmp <- countData[,c("barcode",conditionIndex$X)]

for(i in 2:ncol(countDataTmp))
  sIndex[i-1] <- ShannonIndex(countDataTmp[,i], minCopies)

barplot(sIndex, names.arg = c(as.character(conditionIndex$Group)), 
        col= c(as.factor(conditionIndex$Group)), ylab = "Shannon Index", 
        cex.axis=1.5, cex.names = 1, cex.lab=1.5)
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Shannon Index for Read Complexity}
\scriptsize
<<ComIndex_p>>=
minCopies = 10
@
\begin{figure}\begin{center}
<<ComIndex_10, echo = FALSE,out.width='4in', fig.width=9, fig.height=4>>=
sIndex <- NULL
countDataTmp <- countData[,c("barcode",conditionIndex$X)]

for(i in 2:ncol(countDataTmp))
  sIndex[i-1] <- ShannonIndex(countDataTmp[,i], minCopies)

barplot(sIndex, names.arg = c(as.character(conditionIndex$Group)), 
        col= c(as.factor(conditionIndex$Group)), ylab = "Shannon Index", 
        cex.axis=1.5, cex.names = 1, cex.lab=1.5)
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Shannon Index for Read Complexity}
\scriptsize
<<ComIndex_p100>>=
minCopies = 100
@
\begin{figure}\begin{center}
<<ComIndex_100, echo = FALSE,out.width='4in', fig.width=9, fig.height=4>>=
sIndex <- NULL
countDataTmp <- countData[,c("barcode",conditionIndex$X)]

for(i in 2:ncol(countDataTmp))
  sIndex[i-1] <- ShannonIndex(countDataTmp[,i], minCopies)

barplot(sIndex, names.arg = c(as.character(conditionIndex$Group)), 
        col= c(as.factor(conditionIndex$Group)), ylab = "Shannon Index", 
        cex.axis=1.5, cex.names = 1, cex.lab=1.5)
@
\end{center}\end{figure}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Shannon Index for Read Complexity}
\scriptsize
\begin{figure}\begin{center}
<<ComIndex_dotplot, echo = FALSE,out.width='4in', fig.width=9, fig.height=4>>=
shannon.index <- data.frame(sIndex=sIndex, group = conditionIndex$Group)
shannon.index.library <- shannon.index$sIndex[25]
shannon.index <- shannon.index[-which(shannon.index$group=="Library"), ]
library(ggplot2)
qplot(shannon.index$group, shannon.index$sIndex)+
  theme_classic()+
  labs(x = "Conditions")+
  labs(y = "Shannon Index")+
  ylim(low=0,high=15)+
  geom_hline(yintercept = shannon.index.library, color="red")+
  theme(axis.text.x = element_text(color="black",size=14,
                      angle=0,hjust=0.5,vjust=0.5,face="plain"),
        axis.text.y = element_text(color="black",size=14,
                      angle=90,hjust=0.5,vjust=0.5,face="plain"),  
        axis.title.x = element_text(color="black",size=14,
                      angle=0,hjust=0.5,vjust=0.5,face="plain"),
        axis.title.y = element_text(color="black",size=14,
                      angle=90,hjust=0.5,vjust=0.5,face="plain"),
        plot.title = element_text(color="black", size=14, hjust = 0.5))
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Shannon Index for Read Complexity}
Wilcox test against the library
<<wilcox.text>>=
pval <- rep(0, length(condition)-1)
grp  <- as.character(rep("grp", length(condition)-1))
j    <- 1
for(i in seq_len(length(condition))){
  if(condition[i] != "Library"){
    pval[j] <- wilcox.test(shannon.index[which(shannon.index$group==
       condition[i]),1],mu=shannon.index.library, alternative="less")$p.value
    grp[j]  <- as.character(condition[i])
    j       <- j + 1}
}
data.frame(grp = grp, Wilcox.pval=pval)
@
\end{frame}




\section{Cumulative Abundance}
\begin{frame}[fragile]{Cumulative Abundance}
<<cumfunction, echo=FALSE>>=
cumAbun <- function(x,minCopies, n)
{
    x <- x[which(x > minCopies)]
    x <- x/sum(x)
	x <- sort(x,decreasing =TRUE)
	if(length(x) > n)
	{
		x <- x[1:n]
	}else{
	    y <- rep(0,n)
	    y[1:length(x)] <-x
		x <- y
	}
	for( i in 1:(n-1))
		x[i+1]=x[i+1]+x[i]
	x
}	
n <-500
cAun <- NULL
minCopies = 100
countDataTmp <- countData[,c("barcode",conditionIndex$X)]
for( i in 2:44){ 
  	if (i == 1)
		cAun <-cumAbun(countDataTmp[,i], minCopies, n)
 	else
	cAun <-cbind(cAun, cumAbun(countDataTmp[,i], minCopies,n))
}
cAun <- data.frame(cAun)
cAun$index = seq(1:n)
colnames(cAun) = colnames(countDataTmp[,2:44])
library(ggplot2)
library(RColorBrewer)
library(magrittr)

CulPlot <- function(condition, cAun, conditionIndex)
{
cAumTmp <- cAun[,c(conditionIndex[which(conditionIndex[,2] == condition),]$X)]
cAumTmp$index = seq(1:n)
cAumTmp <- melt(cAumTmp,id='index')
cAumTmp %>% ggplot(aes(x=index, y=value, group = variable, colour = variable)) +
   geom_line(size=1) +
   labs(color = "")+
   labs(x = "") +
   labs(y = "Cumulative Abundance ") +
   labs(title = "", subtitle = "")+
   theme_classic() +
   scale_colour_manual(values= brewer.pal(n=length(unique(cAumTmp$variable)),"Set3")) +
   theme(axis.text.x = element_text(color="black",size=12,angle=0,hjust=0.5,vjust=0.5,face="plain"),
         axis.text.y = element_text(color="black",size=12,angle=90,hjust=0.5,vjust=0.5,face="plain"),  
       axis.title.x = element_text(color="black",size=12,angle=0,hjust=0.5,vjust=0.5,face="plain"),
       axis.title.y = element_text(color="black",size=12,angle=90,hjust=0.5,vjust=0.5,face="plain"),
       plot.title = element_text(color="black", size=12, hjust = 0.5),
       plot.subtitle = element_text(color="black", size=10, hjust = 0.5)) 
}
@
Primary 
\begin{figure}\begin{center}
<<echo = FALSE,out.width='4in', fig.width=7, fig.height=4>>=
CulPlot("Pri",cAun, conditionIndex)
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Cumulative Abundance}
Late Primary 
\begin{figure}\begin{center}
<<echo = FALSE,out.width='4in', fig.width=7, fig.height=4>>=
CulPlot("LPri",cAun, conditionIndex)
@
\end{center}\end{figure}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Cumulative Abundance}
Early Residule 
\begin{figure}\begin{center}
<<echo = FALSE,out.width='4in', fig.width=7, fig.height=4>>=
CulPlot("earRes",cAun, conditionIndex)
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Cumulative Abundance}
Late Residule 
\begin{figure}\begin{center}
<<echo = FALSE,out.width='4in', fig.width=7, fig.height=4>>=
CulPlot("LRes",cAun, conditionIndex)
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Cumulative Abundance}
Non Rcur 
\begin{figure}\begin{center}
<<echo = FALSE,out.width='4in', fig.width=7, fig.height=4>>=
CulPlot("NRcur",cAun, conditionIndex)
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Cumulative Abundance}
Rcur 
\begin{figure}\begin{center}
<<echo = FALSE,out.width='4in', fig.width=7, fig.height=4>>=
CulPlot("Rcur",cAun, conditionIndex)
@
\end{center}\end{figure}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Kullback–Leibler Divergence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Kullback–Leibler Divergence}
\begin{frame}[fragile]{Kullback–Leibler Divergence}
Compute the Kullback–Leibler Divergence for each conditions
regarding to the library.
\begin{equation*}
D_{KL} = \Sigma_i P(i) \log(\frac{P(i)}{Q(i)})
\end{equation*}

\begin{figure}\begin{center}
<<echo = FALSE,out.width='4in', fig.width=9, fig.height=4>>=
countPrevalance <- countData
KL <-NULL
for(i in 2:ncol(countData))
	countPrevalance[,i] =  countPrevalance[,i]/sum(countPrevalance[,i]) 

for(i in 3:ncol(countData))
{
	tmp <- countPrevalance[,i]*log(countPrevalance[,i]/countPrevalance[,2])
	tmp[is.infinite(tmp)] <- 0
	tmp[is.na(tmp)] <- 0
	KL[i-2] = sum(tmp,na.rm=TRUE)
}

KL <- data.frame(KL)
rownames(KL) <- colnames(countData)[3:ncol(countData)]
barplot(KL[conditionIndex$X[-25],], 
        names.arg = c(as.character(conditionIndex$Group[-25])),
        col= c(as.factor(conditionIndex$Group[-25])), ylab = "KL Divergence", 
        cex.axis=1.5, cex.names = 1, cex.lab=1.5)
@
\end{center}\end{figure}
\end{frame}

\begin{frame}[fragile]{Kullback–Leibler Divergence}
\scriptsize
\begin{figure}\begin{center}
<<KL_dotplot, echo = FALSE,out.width='4in', fig.width=7, fig.height=4>>=
KL <- KL[conditionIndex$X[-25],]
KL <- data.frame(KL)
rownames(KL) <- c(as.character(conditionIndex$X[-25]))
KL.divergence <- data.frame(KL=KL, group = conditionIndex$Group[-25])
qplot(KL.divergence$group, KL.divergence$KL)+
  theme_classic()+
  labs(x = "Conditions")+
  labs(y = "Kullback Leibler Divergence")+
  ylim(low=0,high=15)+
  theme(axis.text.x = element_text(color="black",size=14,angle=0,hjust=0.5,vjust=0.5,face="plain"),
         axis.text.y = element_text(color="black",size=14,angle=90,hjust=0.5,vjust=0.5,face="plain"),  
       axis.title.x = element_text(color="black",size=14,angle=0,hjust=0.5,vjust=0.5,face="plain"),
       axis.title.y = element_text(color="black",size=14,angle=90,hjust=0.5,vjust=0.5,face="plain"),
       plot.title = element_text(color="black", size=14, hjust = 0.5))
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Mann–Whitney-Wilcoxon test}
Mann–Whitney-Wilcoxon test for Kullback–Leibler Divergence
\scriptsize
<<MWUtest>>=
pval <- matrix(NA, nrow=length(condition)-1, ncol=length(condition)-1)
condition.tmp = condition[-which(condition=="Library")]
for(i in seq_len(length(condition.tmp)-1)){
  for(j in (i+1):length(condition.tmp))
     pval[i,j]<- wilcox.test(x = KL.divergence[which(KL.divergence$group==
       condition.tmp[i]),1], y = KL.divergence[which(KL.divergence$group==
       condition.tmp[j]),1])$p.value}
rownames(pval) <- condition.tmp
colnames(pval) <- condition.tmp
pval
@
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% L1 norm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{frame}[fragile]{L1-Norm}
%Compute the L1-Norm for each conditions
%regarding to the library.
%\begin{equation*}
%L^1_{KL} = \Sigma_i |P(i) - Q(i)|
%\end{equation*}
%\begin{figure}\begin{center}
%<<echo = FALSE,out.width='4in', fig.width=7, fig.height=4>>=
%L1Norm <- NULL
%for(i in 3:44)
%	L1Norm[i-2] = sum(abs(countPrevalance[,i]-countPrevalance[,2]))
%
%L1Norm <- data.frame(L1Norm)
%rownames(L1Norm) <- colnames(countData)[3:44]
%barplot(L1Norm[conditionIndex$X[-25],], 
%        names.arg = c(as.character(conditionIndex$Group[-25])),
%        col= c(conditionIndex$Group[-25]), ylab = "L1 Norm", cex.axis=1.5, 
%        cex.names = 1, cex.lab=1.5)
%@
%\end{center}\end{figure}
%\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pearson's correlation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Pearson's correlation}
\begin{frame}[fragile]{Pearson's correlation}
\begin{figure}\begin{center}
<<echo = FALSE,out.width='4in', fig.width=9, fig.height=6.5>>=
library("RColorBrewer")
cov_xy   <- cov(countData[,2:ncol(countData)])
sd_x     <- sapply(countData[,2:ncol(countData)], sd, na.rm = TRUE)
sd_x_time_x <- sd_x%*%t(sd_x)
Pcorr    <- cov_xy/sd_x_time_x
xx       <- Pcorr[conditionIndex[,1],conditionIndex[,1]]
library(pheatmap)
annotation_row = data.frame(Condition = conditionIndex[,2])
rownames(annotation_row) = conditionIndex[,1]
pheatmap(xx, annotation_col = annotation_row, annotation_row = annotation_row, 
         color = colorRampPalette(c("blue", "orange", "red"))(50),
         cluster_rows=FALSE, cluster_cols=FALSE)
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Pearson's correlation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Pearson's correlation}
\begin{figure}\begin{center}
<<echo = FALSE,out.width='4in', fig.width=9, fig.height=7>>=
pheatmap(xx, annotation_col = annotation_row, annotation_row = annotation_row, 
         color = colorRampPalette(c("blue", "orange", "red"))(50))
@
\end{center}\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Highdimension mean test
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{frame}[fragile]{High-Dimensional Mean Test}
%Test vectors mean in two sample populations
%<<highmean>>=
%#library(highmean)
%#x <- as.matrix(countData[,c(conditionIndex[which(conditionIndex[,2] == "Library"),]$X)])
%#y <- as.matrix(countData[,c(conditionIndex[which(conditionIndex[,2] == "LRes"),]$X)])
%#apval_Bai1996(t(cbind(x,x)),t(y))
%#apval_aSPU(x,y)
%@

%\end{frame}



\begin{frame}[fragile]{Session information}
<<echo=FALSE,results='asis', include=TRUE>>=
toLatex(sessionInfo(), locale=FALSE)
@ 
<<echo=FALSE>>=
print(paste("Start Time",stdt))
print(paste("End Time  ",date()))
@ 
\end{frame}

\end{document}



