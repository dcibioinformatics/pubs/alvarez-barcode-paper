#include <string>
#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>
#include <istream>
#include <cstdint>
#include <algorithm>
#include <future>
#include <boost/program_options.hpp>
namespace po = boost::program_options;

typedef std::pair<std::string, std::string> input_t;
typedef std::vector<std::tuple<std::string, std::string, int, std::string>> return_t;

auto distance(std::string& x, std::string& y){
  auto dist = 0;
for(auto i = 0; i < 18; i++){
  dist += x[i] != y[i];
}
return dist;
}

auto proc_refs(std::string fname){
  std::ifstream in(fname);
  auto refs = std::vector<input_t>();

  auto line0 = std::string(), line1 = std::string();

  while(getline(in, line0)){
    getline(in, line1);

    line0.erase(0,1);

    refs.push_back(make_pair(line0, line1));
  }
  return refs;
}


auto check_lines(std::vector<input_t> lines, std::vector<input_t>& refs){
  auto ret = return_t();

  for(auto& line : lines){
    auto match = std::string();
    auto dist = 18;
    auto vals = line.second;

    for(auto& x : refs){
      auto new_dist = distance(vals, x.second);
      // shortcut out
      if(new_dist < 2){
        dist = new_dist;
        match = x.first;
        break;
      }
      if(new_dist < dist){
        dist = new_dist;
        match = x.first;
      }
    }

    ret.push_back(std::make_tuple(line.first, match, dist, line.second));
  }

  return ret;
}

// prints out entries
auto resolve(std::vector<std::future<return_t>>& futures){
  for(auto& x : futures){
    auto vals = x.get();

    for(auto& y : vals){
      // ID / Match ID / Distance to Match / Sequence
      std::cout << std::get<0>(y) << "\t" << std::get<1>(y) << "\t" << std::get<2>(y) << "\t" << std::get<3>(y) << std::endl;
    }
  }
  futures.clear();
}


auto process(std::istream& in, std::vector<input_t>& refs, int& max_futures, int& chunk_size){
  auto futures = std::vector<std::future<return_t>>();

  std::stringstream ss;

  auto lines = std::vector<input_t>();

  while(in.peek() != EOF){

    // gather lines in chunks
    while(lines.size() < chunk_size){
      auto line = std::string(), drop = std::string(), id_line = std::string(), id = std::string();

      getline(in, id_line, '\n');
      getline(in, line, '\n');
      getline(in, drop, '\n');
      getline(in, drop, '\n');

      if(!line.empty() && !id_line.empty()){
        ss.clear();
        ss << id_line;
        ss >> id;
        ss >> drop;

        lines.push_back(make_pair(id, line));
      } else {
        break;
      }
    }

    // enough futures? resolve + clear
    if(futures.size() >= max_futures){
      resolve(futures);
    }

    // add future
    futures.push_back(std::async(std::launch::async, check_lines, lines, std::ref(refs)));
    lines.clear();
  }

  // cleanup
  resolve(futures);
}


int main(int argc, char* argv[]){
  auto ref = std::string(), file = std::string();
  auto max_futures = 40, chunk_size = 500;

  auto desc = po::options_description("Allowed options");

  desc.add_options()
    ("help,h", "produce help message")
    ("ref,r", po::value<std::string>(&ref), "reference fasta file")
    ("max-futures,t", po::value<int>(&max_futures), "maximum number of concurrent futures")
    ("chunk_size,c", po::value<int>(&chunk_size), "size of futures")
    ("file,f", po::value<std::string>(&file), "input file");

  auto file_options = po::positional_options_description();
  file_options.add("file", -1);

  auto vm = po::variables_map();

  po::store(po::command_line_parser(argc, argv).options(desc).positional(file_options).run(), vm);
  po::notify(vm);

  if(vm.count("help")){
    std::cout << desc << "\n";
    return 1;
  }

  if(!vm.count("ref")){
    std::cerr << "no reference file specified\n";
    exit(1);
  }

  auto ref_map = proc_refs(vm["ref"].as<std::string>());

  if(vm.count("file")){
    std::cerr << "Mapping file: " << vm["file"].as<std::string>() << "\n";

    std::ifstream stream(vm["file"].as<std::string>());
    process(stream, ref_map, max_futures, chunk_size);
  } else {
    std::cerr << "reading from stdin...\n";
    process(std::cin, ref_map, max_futures, chunk_size);
  }

  return 0;
}
