#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <boost/program_options.hpp>
namespace po = boost::program_options;


auto get_base(std::string& filename) -> std::string {
  auto name = std::regex("^(?:.+\\/)*([^\\/]+)\\.fastq$", std::regex::optimize);
  auto m = std::smatch();

  std::regex_match(filename, m, name);

  if(!m.empty()){
    return m[1].str();
  } else {
    return filename;
  }
}

auto take_line(std::string& line, std::string& id_line, std::string& spacer, std::smatch m, std::ostream& left, std::ostream& right, std::istream& in){
  left << id_line << "\n";
  right << id_line << "\n";

  left << m[1] << "\n";
  right << m[2] << "\n";

  getline(in, line);

  left << "+\n";
  right << "+\n";

  getline(in, line);

  auto seq0 = std::string(), seq1 = std::string();

  std::copy(line.begin(), line.begin() + m[1].length(), back_inserter(seq0));
  std::copy(line.begin() + m[1].length() + spacer.length(), line.end(), back_inserter(seq1));

  left << seq0 << "\n";
  right << seq1 << "\n";
}

auto take_line_same(std::string& line, std::string& id_line, std::string& spacer, std::smatch& m, std::ostream& left, std::ostream& right, std::istream& in){
  if(m[1].length() == m[2].length()){
    left << id_line << "\n";
    right << id_line << "\n";

    left << m[1] << "\n";
    right << m[2] << "\n";

    getline(in, line);

    left << "+\n";
    right << "+\n";

    getline(in, line);

    auto seq0 = std::string(), seq1 = std::string();

    std::copy(line.begin(), line.begin() + m[1].length(), back_inserter(seq0));
    std::copy(line.begin() + m[1].length() + spacer.length(), line.end(), back_inserter(seq1));

    left << seq0 << "\n";
    right << seq1 << "\n";

  } else {

    getline(in, line);
    getline(in, line);
  }
}


auto process(std::string file, std::string spacer, bool same){
  auto base = get_base(file);
  // std::cerr << "base name: " << base << "\n";

  std::ifstream in(file);
  std::ofstream left(base + "-LEFT.fastq");
  std::ofstream right(base + "-RIGHT.fastq");

  std::cerr << "output files: " << base << "-LEFT.fastq" << " " << base << "-RIGHT.fastq\n";

  auto id_line = std::string(), line = std::string();

  auto re = std::string("(.+)" + spacer + "(.+)");
  auto split = std::regex(re, std::regex::optimize);
  auto m = std::smatch();


  if(same){
  // checking every second line out of four... fastq format
    while(getline(in, id_line)){
      getline(in, line);
      std::regex_match(line, m, split);

      if(m.size() < 2){
        getline(in, line);
        getline(in, line);
      } else {
        take_line_same(line, id_line, spacer, m, left, right, in);
      }
    }
  } else {
    while(getline(in, id_line)){
      getline(in, line);
      std::regex_match(line, m, split);

      if(m.size() < 2){
        getline(in, line);
        getline(in, line);
      } else {
        take_line(line, id_line, spacer, m, left, right, in);
      }
    }
  }
}


int main(int argc, char* argv[]){
  auto file = std::string(), spacer = std::string("CG.A");
  auto same = true;

  auto desc = po::options_description("Allowed options");

  desc.add_options()
    ("help,h", "produce help message")
    ("spacer,s", po::value<std::string>(&spacer), "spacer to split by")
    ("same,z", po::value<bool>(&same), "require same size for Left + Right sequences after splitting, default: true")
    ("file,f", po::value<std::string>(&file), "input file");

  auto file_options = po::positional_options_description();

  file_options.add("file", -1);

  auto vm = po::variables_map();
  po::store(po::command_line_parser(argc, argv).options(desc).positional(file_options).run(), vm);
  po::notify(vm);

  if(vm.count("help")){
    std::cout << desc << "\n";
    return 0;
  }

  if(vm.count("file")){
    std::cerr << "Splitting file: " << vm["file"].as<std::string>() << "\n";
    process(vm["file"].as<std::string>(), spacer, same);

  } else {
    std::cerr << "no input file given. Exiting" << std::endl;
    return 1;
  }
}
