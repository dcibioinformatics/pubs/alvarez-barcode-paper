#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <vector>
#include <exception>
#include <algorithm>
#include <boost/program_options.hpp>
#include <future>
#include <regex>

typedef std::tuple<std::string, int, double> entry_t;
typedef std::pair<std::set<std::string>, std::pair<std::string, std::vector<entry_t>>> ret_t;

namespace po = boost::program_options;

auto get_or_else(std::vector<entry_t>& vec, std::string loc, std::tuple<int, double, int>& df){
  auto y = std::find_if(vec.begin(), vec.end(), [&](entry_t& x){return std::get<0>(x) == loc;});
  if(y == vec.end()){
    return df;
  } else {
    auto x0 = std::get<1>(*y); // count
    auto x1 = std::get<2>(*y); // %
    int z =  vec.end() - y; // rank (0 => none, rank 1 is highest)
    return std::make_tuple(x0, x1, z);
  }
}


auto run(std::string file){
  auto line = std::string(), bc0 = std::string(), bc1 = std::string(), cnt = std::string(), key = std::string();
  auto count = 0;

  std::ifstream from(file);

  auto tot = 0;
  auto vec = std::vector<entry_t>();
  auto keys = std::set<std::string>();

  auto re = std::regex("^([0-9]+)\\s+([0-9]+)\\s+([0-9]+)$", std::regex::optimize);
  std::smatch m;

  while(getline(from, line)){
    std::regex_match(line, m, re);

    if(!m.empty()){
      auto bc0 = m[1].str(), bc1 = m[2].str(), cnt = m[3].str();
      count = stoi(cnt);
      key = bc0 + ":" + bc1;
      keys.insert(key);
      vec.push_back(std::make_tuple(key, count, 0.0));
      tot += count;
    }
  }

  for(auto& x : vec){
    std::get<2>(x) = std::get<1>(x) * 1.0 / tot;
  }

  std::sort(vec.begin(), vec.end(), [](entry_t& a, entry_t& b){return std::get<1>(a) < std::get<1>(b);});
  return std::make_pair(keys, std::make_pair(file, vec));
}


auto finish(std::vector<std::string>& files, std::map<std::string, std::vector<entry_t>>& results, std::set<std::string>& keys){
  std::ofstream counts("all-counts.tsv");
  std::ofstream prc("all-percent.tsv");
  std::ofstream rank("all-ranks.tsv");

  counts << "barcode" << ",";
  for(auto file = files.begin(); file != files.end(); file++){
    counts << *file;
    prc << *file;
    rank << *file;
    if(file != files.end() - 1){
      counts << ",";
      prc << ",";
      rank << ",";
    }
  }
  counts << "\n";
  prc << "\n";
  rank << "\n";

  auto no_entry = std::make_tuple(0, 0.0, 0);

  for(auto& key : keys){
    counts << key;
    prc << key;
    rank << key;

    for(auto& x : results){
      auto val = get_or_else(x.second, key, no_entry);

      counts << "," << std::get<0>(val);
      prc << "," << std::get<1>(val);
      rank << "," << std::get<2>(val);
    }
    counts << "\n";
    prc << "\n";
    rank << "\n";
  }
}


auto process(std::vector<std::string>& files, std::vector<std::string>& sample_ids){
  // filename -> barcode -> count, %
  auto res = std::map<std::string, std::vector<entry_t>>();

  auto keys = std::set<std::string>();
  auto futures = std::vector<std::future<ret_t>>();

  for(auto file = files.begin(); file != files.end(); file++){
    futures.push_back(std::async(std::launch::async, run, *file));
  }

  for(auto& x : futures){
    auto y = x.get();
    std::copy(y.first.begin(), y.first.end(), std::inserter(keys, keys.end()));
    res.insert(y.second);
  }

  finish(sample_ids, res, keys);
}


int main(int argc, char* argv[]){
  auto desc = po::options_description("Allowed options");
  auto files = std::vector<std::string>();

  desc.add_options()
    ("help,h", "produce help message")
    ("files,f", po::value<std::vector<std::string>>(&files)->multitoken(), "input files");

  auto positional = po::positional_options_description();
  positional.add("files", -1);

  auto vm = po::variables_map();

  po::store(po::command_line_parser(argc, argv).options(desc).positional(positional).run(), vm);
  po::notify(vm);

  if(vm.count("help")){
    std::cerr << desc << "\n";
    return 1;
  }

  auto sample_ids = std::vector<std::string>();
  auto name_re = std::regex("^(.*)-[^-]*$", std::regex::optimize);
  auto m = std::smatch();

  std::cerr << "\nrunning with files: ";
  for(auto& file : files){
    std::cerr << file << " ";
    std::regex_match(file, m, name_re);
    sample_ids.push_back(m[1].str());
  }
  std::cerr << "\n";

  process(files, sample_ids);
}
