## set working directory
setwd("~/Projects/Alarez/alarezalign/2017Copy/")

## running counting 
#system("../qualityCheck/qcheck-pipeline")

## get sample file names
spacierCountFiles <- list.files(path = ".", pattern = "*spacier.tsv")
numSample <- length(spacierCountFiles)

## read in results
for( i in seq_len(numSample)){
  spacierCountTmp <- read.csv(spacierCountFiles[i], header=TRUE)
  if(i == 1)
    spacierCount <- spacierCountTmp
  else
    spacierCount <- merge(spacierCount, spacierCountTmp, by = "Spacier")
}

## using short names for data column
shortsampleNames <- NULL
sampleNameSplit<- strsplit(spacierCountFiles, split=c("-", "_"), fixed = TRUE)
for(i in seq_len(numSample))
  shortsampleNames[i] <- sampleNameSplit[[i]][1]

spacierCount[,numSample+2] <- rowSums(spacierCount[,2:(numSample+1)])
colnames(spacierCount) <- c("spacier", shortsampleNames, "sum")
spacierCount <- spacierCount[order(spacierCount$sum,decreasing=TRUE),]
spacierCount[nrow(spacierCount)+1, 2:ncol(spacierCount)] <- colSums(spacierCount[,2:ncol(spacierCount)])
## output top spaciers
spacierCount[1:15,c(1,28)]
write.csv(spacierCount, file = "./spacierCount.csv")


## get sample file names
phredCountFiles <- list.files(path = ".", pattern = "*phred.tsv")
numSample <- length(phredCountFiles)

## read in results
for( i in seq_len(numSample)){
  phredCountTmp <- read.csv(phredCountFiles[i], header=TRUE)
  if(i == 1)
    phredCount <- phredCountTmp
  else
    phredCount <- merge(phredCount, phredCountTmp, by = "Score")
}

## using short names for data column
shortsampleNames <- NULL
sampleNameSplit<- strsplit(phredCountFiles, split=c("-", "_"), fixed = TRUE)
for(i in seq_len(numSample))
  shortsampleNames[i] <- sampleNameSplit[[i]][1]

phredCount[,numSample+2] <- rowSums(phredCount[,2:(numSample+1)])
colnames(phredCount) <- c("phred", shortsampleNames, "sum")
phredCount <- phredCount[order(phredCount$sum,decreasing=TRUE),]

## output top phreds
phredCount[1:15,c(1,28)]
