
// reading a text file
#include <iostream>
#include <fstream>
#include <string>
#include <map>
using namespace std;

int main (int argc, char *argv[]) {

  map<string, int> spacierCount;
  map<int, int> phredScore;
  ofstream spacierfile (argv[2]);
  ofstream phredScorefile (argv[3]);

  if ( argc != 4 ) // argc should be 2 for correct execution
    // We print argv[0] assuming it is the program name
    cout<<"usage: "<< argv[0] <<" <filename>\n";
  else {
    string line;
    ifstream myfile (argv[1]);
    if (myfile.is_open())
    {
      while ( getline (myfile,line) )
      {
        getline (myfile,line);
        // processing spacier
        if(spacierCount.find(line.substr(17,4)) == spacierCount.end())
          spacierCount[line.substr(17,4)] = 1;
        else
          spacierCount[line.substr(17,4)] ++;
        getline (myfile,line);
        getline (myfile,line);
        for(int i = 0; i < 4; i++){
          int intPhred = line.at(17+i);
          if(phredScore.find(intPhred) == phredScore.end())
            phredScore[intPhred] = 1;
          else
            phredScore[intPhred] ++;
        }
      }
      myfile.close();
    }

    else cout << "Unable to open file";
  }
 
  spacierfile << "Spacier, " << "Count" <<endl;
  for(auto it:spacierCount)
  {
     spacierfile << it.first << ", " << it.second << endl;
  }
   
  phredScorefile << "Score, " << "Count" <<endl;
  for(auto it: phredScore)
  {
    phredScorefile << it.first << ", " << it.second << endl;
  }
  
  
  return 0;
}










