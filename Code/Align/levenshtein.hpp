#ifndef LEVENSHTEIN_H
#define LEVENSHTEIN_H

#include <algorithm>

namespace levenshtein {
  // from rosettacode.org: https://rosettacode.org/wiki/Levenshtein_distance#C.2B.2B
  // written by Martin Ettl, 2012-10-05
  // adapted to c++14
  auto distance(const std::string& s0, const std::string& s1){
    const auto m = s0.size();
    const auto n = s1.size();

    if(m == 0) return n;
    if(n == 0) return m;

    auto* costs = new size_t[n + 1];

    for(auto k = 0; k <= n; k++){
      costs[k] = k;
    }

    auto i = size_t(0);
    for(auto it0 = s0.cbegin(); it0 != s0.cend(); ++it0, ++i){
      costs[0] = i + 1;
      auto corner = i;

      auto j = size_t(0);
      for(auto it1 = s1.cbegin(); it1 != s1.cend(); ++it1, ++j){

        auto upper = costs[j + 1];
        costs[j + 1] = (*it0 == *it1)? corner : std::min({costs[j], upper, corner}) + 1;
        corner = upper;
      }
    }

    auto result = costs[n];
    delete [] costs;
    return result;
  }


  auto ext_distance(int& mm_penalty, int& gap_penalty, const std::string& s0, const std::string& s1){
    const auto m = s0.size();
    const auto n = s1.size();

    if(m == 0) return n;
    if(n == 0) return m;

    auto* costs = new size_t[n + 1];

    for(auto k = 0; k <= n; k++){
      costs[k] = k;
    }

    auto i = size_t(0);
    for(auto it0 = s0.cbegin(); it0 != s0.cend(); ++it0, ++i){
      costs[0] = i + 1;
      auto corner = i;

      auto j = size_t(0);
      for(auto it1 = s1.cbegin(); it1 != s1.cend(); ++it1, ++j){

        auto upper = costs[j + 1];
        //  corner(i-1, j)     upper(i - 1, j + 1)
        //  last(i, j)         current(i, j+1)
        costs[j + 1] = (*it0 == *it1)? corner : std::min({costs[j] + gap_penalty, upper + gap_penalty, corner + mm_penalty});
        corner = upper;
      }
    }

    auto result = costs[n];
    delete [] costs;
    return result;
  }

}

#endif
