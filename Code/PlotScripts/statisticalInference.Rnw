\documentclass[10pt,xcolor=x11names,compress]{beamer}

\usepackage{lmodern}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{booktabs}
\usepackage[round]{natbib}
\renewcommand{\newblock}{} % Get natbib and beamer working together: http://tex.stackexchange.com/questions/2569/beamer-and-natbib
\usetheme{Copenhagen}
%\VignetteEngine{knitr::knitr}
%\VignetteIndexEntry{fastJT}

\newcommand{\R}{\texttt{R}}
\newcommand{\pkgname}{\texttt{fastJT}}
\newcommand{\Rcpp}{\texttt{Rcpp}}
\newcommand{\openmp}{\texttt{OpenMP}}

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]

\setbeamertemplate{enumerate item}{\insertenumlabel}
\setbeamertemplate{enumerate subitem}{\alph{enumii}.}


\begin{document}
%\SweaveOpts{concordance=TRUE}
<<setup1, echo=FALSE>>=
require(knitr)
library(RColorBrewer)
library(reshape2)
library(stringr)
suppressMessages(library(dplyr, warn.conflicts = FALSE, quietly=TRUE))
suppressMessages(library(plyr, warn.conflicts = FALSE, quietly=TRUE))
suppressMessages(library(tidyverse, warn.conflicts = FALSE, quietly=TRUE))
suppressMessages(library(vegan, warn.conflicts = FALSE, quietly=TRUE))
library(pheatmap)
@

<<setup2, echo=FALSE>>=
options(width=80)  # make the printing fit on the page
set.seed(1121)     # make the results repeatable
stdt<-date()
library(RColorBrewer)
library(reshape2)
@

%------------------------------------------------------------
%slide1: Project name, author,
%------------------------------------------------------------
\begin{frame}
\title{Statistical Inference on Barcode Compositional Complexity}
\titlepage
\end{frame}


\scriptsize
\section{Load Count Data}
\begin{frame}[fragile]{Read in Barcode Count Data}
Setup
\scriptsize
<<setup>>=
wd       <- "~/Work_old/Works/05_Alvarez/alvarez_mouse_cellecta_crispr_sreen/alvarez-paper/"
cF_16    <- "Data/20161014_HiSeq-42of42-with_repeats-revised_counts.csv"
metaF_16 <- "Data/Alvarez_Cohort.csv"
cF       <- "Data/all-counts_2017.tsv"
metaF    <- "Data/Alvarez_Cohort_2017_add_to2016.csv"
# read in count data and meta info
cDat_16 <- read.csv(file.path(wd, cF_16), header = TRUE, 
                    stringsAsFactors=FALSE, check.names=FALSE)
meta_16 <- read.csv(file.path(wd, metaF_16), header=TRUE, stringsAsFactors=FALSE)
cDat    <- read.csv(file.path(wd, cF), header = TRUE, 
                    stringsAsFactors=FALSE, check.names=FALSE)
meta    <- read.csv(file.path(wd, metaF), header=TRUE, stringsAsFactors=FALSE)
meta_16$X <-  gsub("\\.", "-", meta_16$X)
sNames <- colnames(cDat)
for(i in 1:length(sNames))
      sNames[i] <- strsplit(sNames[i],"_")[[1]][1] 
sNames[2] <- "1668"
meta[1,1]  <- "1669-PAR"
colnames(cDat) <- sNames
@
\end{frame}


\begin{frame}[fragile]{Read in Barcode Count Data}
Read in count and manifest files
\scriptsize
<<merge>>=
meta$MET <-NA
cDat_add <- cDat[,c("barcode", meta[,1])]
meta     <- rbind(meta_16, meta)
cDat     <- join(cDat_16, cDat_add, by ="barcode", type="full")
cDat[is.na(cDat)] <- 0
colnames(meta)    <- c("sNames", "Group", "MET")
rownames(cDat)    <- cDat[,"barcode"]
cDat              <- cDat[, meta$sNames]
dim(cDat)
minCopies <- 1
cDat[cDat<minCopies] <- 0
cDat <- cDat[rowSums(cDat)>1,]
dim(cDat)
@
\end{frame}


\section{Proprocessing}
\begin{frame}[fragile]{Column Indexes for Each Stage Tumors}
\scriptsize
<<SampleIndex>>=
PriInd      <- which(meta$Group=="Pri")
LPriInd     <- which(meta$Group=="LPri")
LibInd      <- which(meta$Group=="Library")
earResInd   <- which(meta$Group=="earRes")
LResInd     <- which(meta$Group=="LRes")
RCurInd     <- which(meta$Group=="Rcur")
RCurMetInd  <- which(meta$Group=="Rcur" & meta$MET>10)
RCurNoMetInd<- which(meta$Group=="Rcur" & meta$MET<10)
@
\end{frame}

\section{Injected Cells Barcode Proportions}
\begin{frame}[fragile]{Barcode Proportion Range for Injected Cells}
Number of Unique Barcodes of the Injected Cells Sample:
<<NumUniqueBarLib>>=
ubarNumLib <- sum(cDat[,LibInd] > 1)
ubarNumLib
@

Barcode Proportion Range of the Injected Cells Sample:
<<ProportionUniqueBarLib>>=
InjectedCount <- cDat[cDat[,LibInd] > 1, LibInd]
summary(InjectedCount/sum(InjectedCount))
@
\end{frame}


\section{Unique Barcodes}
\begin{frame}[fragile]{Number of Unique Barcodes (Mean and Range)}
\tiny
Primary Tumors:
<<NumberOfUniqueBarcodesP>>=
ubarNumPri <- colSums(cDat[,PriInd] > 1)
mean(ubarNumPri)
range(ubarNumPri)
@
Early Residual Tumors:
<<NumberOfUniqueBarcodesER>>=
ubarNumER<- colSums(cDat[,earResInd] > 1)
mean(ubarNumER)
range(ubarNumER)
@
\end{frame}

\begin{frame}[fragile]{Number of Unique Barcodes (Mean and Range)}
\tiny
Late Residual Tumors:
<<NumberOfUniqueBarcodesLR>>=
ubarNumLR<- colSums(cDat[,LResInd] > 1)
mean(ubarNumLR)
range(ubarNumLR)
@
Recurrent Tumors:
<<NumberOfUniqueBarcodesRcur>>=
ubarNumRe<- colSums(cDat[,RCurInd] > 1)
mean(ubarNumRe)
range(ubarNumRe)
@
\end{frame}

\begin{frame}[fragile]{Compare Number of Unique Barcodes}
\tiny
Primary Tumors VS 4 weeks Residual Tumors
<<tTestUniqBarNumPriERes>>=
t.test(ubarNumPri, ubarNumER)
@
Primary Tumors VS 8 weeks Residual Tumors
<<tTestUniqBarNumPriLRes>>=
t.test(ubarNumPri, ubarNumLR)
@
\end{frame}


\begin{frame}[fragile]{Compare Number of Unique Barcodes}
\tiny
4 weeks Residual Tumors VS 8 weeks Residual Tumors
<<tTestUniqBarNumERLR>>=
t.test(ubarNumER, ubarNumLR)
@
\end{frame}


\section{Shannon Diversity Index}
\begin{frame}[fragile]{Compute Shannon Diversity Indexes}
\tiny
<<ShannonDiversityIndex>>=
ShannonIndex <- function(i, Dat, minCopies)
{  
    cDat1 <- Dat[,i]
    cDat1 <- cDat1[which(cDat1 > minCopies)]
    cDat1 <- cDat1/sum(cDat1)
    -sum(cDat1*log(cDat1))
}
sIndex <- sapply(1:ncol(cDat), ShannonIndex, Dat=cDat, minCopies=minCopies)
sIndex <- data.frame(sIndex)
rownames(sIndex) <- meta$sNames
t(sIndex)
@
\end{frame}


\begin{frame}[fragile]{Compare Shannon Diversity Indexes}
\tiny
Primary Tumors VS 4 weeks Residual Tumors
<<tTestSIndexPriER>>=
t.test(sIndex[PriInd,], sIndex[earResInd,])
@
Primary Tumors VS 8 weeks Residual Tumors
<<tTestSIndexPriLR>>=
t.test(sIndex[PriInd,], sIndex[LResInd,])
@
\end{frame}

\begin{frame}[fragile]{Compare Shannon Diversity Indexes}
\tiny
4 weeks Residual Tumors VS 8 weeks Residual Tumors
<<tTestSIndexERLR>>=
t.test(sIndex[earResInd,], sIndex[LResInd,])
@
\end{frame}

\begin{frame}[fragile]{Two-way ANOVA test for Shannon Diversity Index Changes}
\scriptsize
Shannon diversity index changes from primary tumor to 4 weeks residual tumors VS
from 4 week residual tumors to 8 weeks residual tumors
<<ANOVASIndexPRiER_ERLR>>=
sIndChange <- data.frame(
                         group=c(rep("primary to early residual", 12), 
                                 rep("early residual to late residual",12)),
                         time=c(rep(1,6),rep(2,6),rep(1,6), rep(2,6)), 
                         response=c(sIndex[PriInd,], sIndex[earResInd,], 
                         sIndex[earResInd,],sIndex[LResInd,]))
res <- aov(response ~ group + time + group:time, data = sIndChange)
summary(res)
@
\end{frame}



\section{Cumulative Abundance}
\begin{frame}[fragile]{Compute Cumulative Abundance}
\tiny
<<CumulativeAbundance>>=
cumAbun <- function(i, cDat, minCopies, n)
{
   topCDat  <- cDat[order(-cDat[,i]), i][1:n]/
               sum(cDat[which(cDat[,i] > minCopies),i])
   topBarID <- cDat[order(-cDat[,i]), 1][1:n]
   cSum     <- cumsum(topCDat)
   return(list(cSum=cSum, topBarID=topBarID))
}
n <- 1000
for( i in 1:ncol(cDat)){ 
  	if (i == 1){
		cAun <- cumAbun(i, cDat, minCopies, n)$cSum
		cAunBarID <- cumAbun(i, cDat, minCopies, n)$topBarID
	}else{
	    cAun <- cbind(cAun, cumAbun(i, cDat, minCopies, n)$cSum)
		cAunBarID <- cbind(cAunBarID, cumAbun(i, cDat, minCopies, n)$topBarID)
	}
}
colnames(cAun) <- meta$sNames
@
\end{frame}


\begin{frame}[fragile]{Average Number of Most Abundance Barcodes Composing 50\% of the Reads for Each Tumor Stage}
\tiny
Injected Cells
<<NumBarMosAbunReadsLib>>=
sum(cAun[, LibInd] < 0.5)
@
Primary Tumors
<<NumBarMosAbunReadsPri>>=
mean(colSums(cAun[, PriInd] < 0.5))
@
4 Week Residual Tumors
<<NumBarMosAbunReadsER>>=
mean(colSums(cAun[, earResInd] < 0.5))
@
8 Week Residual Tumors
<<NumBarMosAbunReadsLR>>=
mean(colSums(cAun[, LResInd] < 0.5))
@
\end{frame}



\begin{frame}[fragile]{Compare Number of Most Abundance Barcodes Composing 50\% of the Reads}
\tiny
Primary Tumors VS 4 weeks Residual Tumors
<<tTesttop50PriER>>=
t.test(colSums(cAun[, PriInd] < 0.5), colSums(cAun[, earResInd] < 0.5))
@
Primary Tumors VS 8 weeks Residual Tumors
<<tTesttop50PriLR>>=
t.test(colSums(cAun[, PriInd] < 0.5), colSums(cAun[, LResInd] < 0.5))
@
\end{frame}

\begin{frame}[fragile]{Compare Number of Most Abundance Barcodes Composing 50\% of the Reads}
\tiny
4 weeks Residual Tumors VS 8 weeks Residual Tumors
<<tTesttop50ERLR>>=
t.test(colSums(cAun[, earResInd] < 0.5), colSums(cAun[, LResInd] < 0.5))
@
\end{frame}



\section{Most Abundant Barcode}
\begin{frame}[fragile]{Read Proportion of the Most Abundant Barcode}
\tiny
Injected Cells
<<MosAbunReadsLib>>=
cAun[1, LibInd]
@
Primary Tumors
<<MosAbunReadsPri>>=
mean(cAun[1, PriInd])
range(cAun[1, PriInd])
@
4 Week Residual Tumors
<<MosAbunReadsER>>=
mean(cAun[1, earResInd])
range(cAun[1, earResInd])
@
\end{frame}

\begin{frame}[fragile]{Read Proportion of the Most Abundant Barcode}
\tiny
8 Week Residual Tumors
<<MosAbunReadsLR>>=
mean(cAun[1, LResInd])
range(cAun[1, LResInd])
@
\end{frame}


\section{Barcode $8240:8518$ Proportion}
\begin{frame}[fragile]{Barcode $8240:8518$ Reads Proportion in Each Tumor Stage}
\tiny
Injected Cells
<<barcode8240Lib>>=
cDat["8240:8518",LibInd]/sum(cDat[,LibInd])
@
Primary Tumors
<<barcode8240Pri>>=
rowMeans(cDat["8240:8518",PriInd]/colSums(cDat[,PriInd]))
range(cDat["8240:8518",PriInd]/colSums(cDat[,PriInd]))
@

4 weeks Residual Tumors
<<barcode8240ER>>=
rowMeans(cDat["8240:8518",earResInd]/colSums(cDat[,earResInd]))
range(cDat["8240:8518",earResInd]/colSums(cDat[,earResInd]))
@
\end{frame}

\begin{frame}[fragile]{Barcode $8240:8518$ Reads Proportion in Each Tumor Stage}
\tiny
8 weeks Residual Tumors
<<barcode8240LR>>=
rowMeans(cDat["8240:8518", LResInd]/colSums(cDat[, LResInd]))
range(cDat["8240:8518", LResInd]/colSums(cDat[, LResInd]))
@
\end{frame}


\begin{frame}[fragile]{Compare Barcode $8240:8518$ Reads Proportion}
\tiny
Primary Tumors VS 4 weeks Residual Tumors
<<tTesttop1PriER>>=
t.test(cDat["8240:8518",PriInd]/colSums(cDat[,PriInd]),
       cDat["8240:8518",earResInd]/colSums(cDat[,earResInd]))
@
Primary Tumors VS 8 weeks Residual Tumors
<<tTesttop1PriLR>>=
t.test(cDat["8240:8518",PriInd]/colSums(cDat[,PriInd]), 
       cDat["8240:8518", LResInd]/colSums(cDat[, LResInd]))
@
\end{frame}

\begin{frame}[fragile]{Compare Barcode $8240:8518$ Reads Proportion}
\tiny
4 weeks Residual Tumors VS 8 weeks Residual Tumors
<<tTesttop1ERLR>>=
t.test(cDat["8240:8518",earResInd]/colSums(cDat[,earResInd]),
       cDat["8240:8518", LResInd]/colSums(cDat[, LResInd]))
@
\end{frame}



\section{Recurrent Time for MET and Non-MET Amplification}
\begin{frame}[fragile]{Preproces Recurrent Time Data}
\tiny
<<loadDataRETime>>=
library(xlsx)
RecurTime <- read.xlsx(file.path(wd, "./Data/recurrenceTime.xlsx"),
                       sheetName="Sheet1", stringsAsFactors=FALSE)
RecurTime <- RecurTime[,c(3,4)]
colnames(RecurTime) <- c("ID", "time")
rownames(meta) <- meta$sNames
RecurTime$MET <- meta[RecurTime$ID,"MET"] > 10
RecurTime$METValue <- meta[RecurTime$ID,"MET"] > 10
RecurTime[!RecurTime$MET, "MET"] <-"Non-MET"
RecurTime[RecurTime$MET==TRUE, "MET"] <-"MET"
RecurTime$Event = 1
RecurTime
@
\end{frame}


\begin{frame}[fragile]{Kaplan-Meier Plots for Time to Recurrence}
\tiny
<<KMplotRecurTime, out.width='4in', fig.width=7, fig.height=4>>=
library(survival)
library(ggfortify)
fit <- survfit(Surv(time, Event)~MET, data=RecurTime)
autoplot(fit)
@
\end{frame}


\begin{frame}[fragile]{Analysis of Recurrence Association with Met Amplification Status}
\tiny
<<survCompare>>=
test <- coxph(Surv(time, Event)~METValue, data=RecurTime)
summary(test)
@
\end{frame}



\section{Jensen-Shannon divergence}
\begin{frame}[fragile]{Jensen-Shannon divergence}
Compute Jensen-Shannon divergence
<<jsd>>=
cDatNorm    <- cDat
for(i in 1:ncol(cDat))
    cDatNorm[,i] <- cDat[,i]/sum(cDat[,i])
JSD <- function(x,y){
    dat <- cbind(x,y)
    dat <- dat[which(x!=0 &y!=0),]
    sum(dat[,1]*log(dat[,1]/dat[,2])+ dat[,2]*log(dat[,2]/dat[,1]), na.rm=TRUE)/2
}
JS <- matrix(rep(0, ncol(cDatNorm)^2), ncol=ncol(cDatNorm))
for(i in 1:(ncol(cDatNorm)-1)){
   for(j in (i+1):ncol(cDatNorm)){
    JS[i,j] <- JSD(cDatNorm[,i], cDatNorm[,j])
    JS[j,i] <- JS[i,j]
}}
Pcorr       <- abs(JS)
rownames(Pcorr) <- colnames(cDatNorm)
colnames(Pcorr) <- colnames(cDatNorm)
@
\end{frame}


\begin{frame}[fragile]{Jensen-Shannon divergence primary cells}
Compare Jensen-Shannon divergence
<<jsd.stat1>>=
jsd.pri.pri <- unique(c(Pcorr[PriInd, PriInd]))
jsd.pri.pri <- jsd.pri.pri[jsd.pri.pri!=0]
t.test(jsd.pri.pri, mu=1, alternative = "two.sided")
@
\end{frame}

\begin{frame}[fragile]{Jensen-Shannon divergence early residual desimilarity to primary}
Compare Jensen-Shannon divergence
<<jsd.stat2>>=
jsd.earRes.pri <- unique(c(Pcorr[earResInd, PriInd]))
t.test(jsd.earRes.pri, jsd.pri.pri)
@
\end{frame}

\begin{frame}[fragile]{Jensen-Shannon divergence late residual  desimilarity to primary }
Compare Jensen-Shannon divergence
<<jsd.stat3>>=
jsd.lateRes.pri <- unique(c(Pcorr[LResInd, PriInd]))
t.test(jsd.lateRes.pri,jsd.pri.pri)
@
\end{frame}

\begin{frame}[fragile]{Jensen-Shannon divergence early residual desimilarity to primary VS late residual  desimilarity to primary }
Compare Jensen-Shannon divergence
<<jsd.stat4>>=
t.test(jsd.earRes.pri, jsd.lateRes.pri)
@
\end{frame}







\begin{frame}[fragile]{Session information}
<<echo=FALSE,results='asis', include=TRUE>>=
toLatex(sessionInfo(), locale=FALSE)
@ 
<<echo=FALSE>>=
print(paste("Start Time",stdt))
print(paste("End Time  ",date()))
@ 
\end{frame}

\end{document}



