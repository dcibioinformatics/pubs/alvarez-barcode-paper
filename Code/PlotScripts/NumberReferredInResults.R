wd       <- "/home/jl354@dhe.duke.edu/Work_old/Works/05_Alvarez/alvarez_mouse_cellecta_crispr_sreen/alvarez-paper/"
source(file.path(wd, "Code/PlotScripts/code2/dataNeeded.R"))

## averge number of unique barcode 
uniBarNum_Lib_Pri[2:length(uniBarNum_Lib_Pri)]
# [1] 4390 4629 3063 5648 5276 4260
summary(uniBarNum_Lib_Pri[2:length(uniBarNum_Lib_Pri)])
#    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
#   3063    4292    4510    4544    5114    5648 


sum(cAbunPri_mean < 0.5)
#[1] 54
sum(cAbunLib < 0.5)
# [1] 700

summary(colSums(cAun_Pri<0.5)/700)
#   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
#0.05571 0.07286 0.07500 0.07976 0.08357 0.11429 

summary(as.vector(PcorPriLib[,2:7]))
#  Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
# 0.6366  0.7188  0.7897  0.7809  0.8646  0.8908       6 

sum(cAbunearRes_mean < 0.5)
#[1] 15
sum(cAbunLRes_mean < 0.5)
#[1] 5

rowMeans(piePri["8240:8518",]/colSums(piePri))
# 8240:8518 
# 0.08397342 
piePri["8240:8518",]/colSums(piePri)
#              2053-L     2064-L     2050-L    2055-L     2077-L    2077-R
#8240:8518 0.08540307 0.04527812 0.05087691 0.1278597 0.09066829 0.1037544




Ratio82408518 <- pieResEL["8240:8518",]/colSums(pieResEL)
#2054-L    2113-R    2113-L    2115-L   2062-L    2059-R   2046-L
#8240:8518 0.0417455 0.1959968 0.2135114 0.1275792 0.126439 0.3410411 0.104196
#             2068-L    2068-R    2110-L   2071-L   2070-L
#8240:8518 0.1281219 0.1782574 0.3258875 0.141637 0.492186
summary(unlist(c(Ratio82408518[1:6])))
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
#0.04175 0.12672 0.16179 0.17439 0.20913 0.34104 
summary(unlist(c(Ratio82408518[7:12])))
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
#0.1042  0.1315  0.1599  0.2284  0.2890  0.4922 
t.test(unlist(c(Ratio82408518[1:6])), unlist(c(Ratio82408518[7:12])))
#data:  unlist(c(Ratio82408518[1:6])) and unlist(c(Ratio82408518[7:12]))
#t = -0.72486, df = 8.7556, p-value = 0.4875
#alternative hypothesis: true difference in means is not equal to 0
#95 percent confidence interval:
# -0.2232250  0.1152341
#sample estimates:
#mean of x mean of y 
#0.1743855 0.2283810 

## Figure 2 BCD
# t-test on the difference among injected cell/primary tumors
# for the following three chararistics
# (1) barcode number
# (2) Shannon index
# (3) barcode distribution (top barcodes ratio)
t.test(uniBarNum_Lib_Pri[1], uniBarNum_Lib_Pri[2:length(uniBarNum_Lib_Pri)])
# can not perform this t.test on a single observation on (1) and (2) 
pvalLib_pri <- NULL
for(i in 1:6){
    pvalLib_pri[i] <- wilcox.test(libcDat$libcDat,pricDat[,i])$p.value
}
pvalLib_pri
#[1] 0 0 0 0 0 0

## Figure 3 BCD
# t-test on the difference among primary/early residual/late residual tumors
# from the following three chararistics
# (1) barcond number 
# (2) Shannon index 
# (3) barcode distribution (top barcodes ratio)

# Unique barcode number 
## primary VS early residual
t.test(uniBarPriResEL$Primary, uniBarPriResEL$earRes)
#    Welch Two Sample t-test
#
#data:  uniBarPriResEL$Primary and uniBarPriResEL$earRes
#t = 3.9058, df = 6.5391, p-value = 0.006695
#alternative hypothesis: true difference in means is not equal to 0
#95 percent confidence interval:
#  639.1219 2673.8781
#sample estimates:
#mean of x mean of y 
# 5312.333  3655.833 

## primary VS late residual
t.test(uniBarPriResEL$Primary, uniBarPriResEL$LRes)
#    Welch Two Sample t-test
#
#data:  uniBarPriResEL$Primary and uniBarPriResEL$LRes
#t = 5.8039, df = 5.249, p-value = 0.001819
#alternative hypothesis: true difference in means is not equal to 0
#95 percent confidence interval:
# 1304.689 3327.311
#sample estimates:
#mean of x mean of y 
# 5312.333  2996.333 

## early residual VS late residual
t.test(uniBarPriResEL$earRes, uniBarPriResEL$LRes)
#    Welch Two Sample t-test
#
#data:  uniBarPriResEL$earRes and uniBarPriResEL$LRes
#t = 3.9149, df = 6.5409, p-value = 0.006617
#alternative hypothesis: true difference in means is not equal to 0
#95 percent confidence interval:
#  255.4222 1063.5778
#sample estimates:
#mean of x mean of y 
# 3655.833  2996.333 

# Shannon index
## primary VS early residual
t.test(sIndexPriResEL$Primary, sIndexPriResEL$earRes)
#    Welch Two Sample t-test
#
#data:  sIndexPriResEL$Primary and sIndexPriResEL$earRes
#t = 6.3623, df = 6.6253, p-value = 0.0004765
#alternative hypothesis: true difference in means is not equal to 0
#95 percent confidence interval:
# 0.7064568 1.5576997
#sample estimates:
#mean of x mean of y 
# 5.857271  4.725193

## primary VS late residual
t.test(sIndexPriResEL$Primary, sIndexPriResEL$LRes)
#    Welch Two Sample t-test
#
#data:  sIndexPriResEL$Primary and sIndexPriResEL$LRes
#t = 8.7276, df = 6.1002, p-value = 0.0001142
#alternative hypothesis: true difference in means is not equal to 0
#95 percent confidence interval:
# 1.337717 2.374314
#sample estimates:
#mean of x mean of y 
# 5.857271  4.001256 

## early residual VS late residual
t.test(sIndexPriResEL$earRes, sIndexPriResEL$LRes)
#Welch Two Sample t-test
#
#data:  sIndexPriResEL$earRes and sIndexPriResEL$LRes
#t = 2.7798, df = 9.6154, p-value = 0.02015
#alternative hypothesis: true difference in means is not equal to 0
#95 percent confidence interval:
# 0.1405143 1.3073604
#sample estimates:
#mean of x mean of y 
# 4.725193  4.001256 


# top barcodes ratio 
## primary VS early residual using KS test
#pricDat
piePri  <- cDat[, PriInd]
pieResE <- cDat[, earResInd]
pieResL <- cDat[, LResInd]

topNum <- 100
topIndex <- list()
for(i in 1:6)
    topIndex <- list(topIndex, order(-piePri[,i])[1:topNum])
for(i in 1:6)
    topIndex <- list(topIndex, order(-pieResE[,i])[1:topNum])
for(i in 1:6)
    topIndex <- list(topIndex, order(-pieResL[,i])[1:topNum])

topBarcode <- as.vector(unique(unlist(topIndex)))

pricDat <- piePri[topBarcode, ]
pieResE <- pieResE[topBarcode, ]
pieResL <- pieResL[topBarcode, ]

## primary VS early residual
pvalPriResE <- matrix(rep(0,36),ncol=6)
for(i in 1:6){
    for(j in 1:6){
        pvalPriResE[i,j] <- wilcox.test(pricDat[,i], pieResE[,j])$p.value
    }
}
pvalPriResE
## consider all barcode in the library
#              [,1]          [,2]         [,3]          [,4]          [,5]
# [1,]  1.742594e-71  5.004488e-56 1.031362e-25  9.704050e-65  3.713021e-52
# [2,]  1.515228e-90  4.632237e-73 1.787794e-37  5.631755e-83  1.175608e-68
##[3,]  4.448694e-01  1.666160e-01 2.396043e-11  8.802179e-01  4.850080e-02
# [4,] 1.256101e-172 3.211788e-148 4.169116e-95 2.841148e-162 3.486727e-142
# [5,] 9.182719e-157 1.130475e-133 2.097266e-83 6.481229e-147 8.619428e-128
# [6,]  1.501385e-65  7.813896e-51 3.331962e-22  4.008332e-59  3.915404e-47
#              [,6]
# [1,] 1.801052e-132
# [2,] 4.983699e-158
# [3,]  8.734292e-14
# [4,] 3.818072e-261
# [5,] 5.345637e-242
# [6,] 1.227977e-124


## consider only the top 500 most abundant barcodes
#             [,1]         [,2]         [,3]         [,4]         [,5]
#[1,] 1.210361e-51 7.160141e-48 2.083297e-39 3.900679e-51 7.103908e-60
#[2,] 1.101063e-41 2.478499e-39 5.041557e-31 4.074313e-42 3.236963e-50
#[3,] 2.185183e-23 1.460819e-20 6.886870e-16 1.319341e-22 5.410415e-28
#[4,] 2.031194e-41 3.093558e-38 2.707385e-30 1.893802e-41 6.207860e-50
#[5,] 6.228761e-44 3.691036e-41 7.595233e-33 5.252435e-44 1.068194e-52
#[6,] 1.617262e-29 2.954344e-27 1.507124e-20 1.095072e-29 2.695840e-37
#              [,6]
#[1,] 9.330733e-101
#[2,]  3.994912e-90
#[3,]  1.605195e-55
#[4,]  3.629540e-92
#[5,]  1.453146e-95
#[6,]  1.719884e-76

## consider only the top 100 most abundant barcodes
#             [,1]         [,2]        [,3]         [,4]         [,5]
#[1,] 0.0005649669 0.0001765871 0.005489468 0.0001033549 1.568791e-06
#[2,] 0.0094274157 0.0045896450 0.053931168 0.0031151051 1.250773e-04
#[3,] 0.0950485379 0.0505300774 0.273810191 0.0419013296 3.829796e-03
#[4,] 0.0062344182 0.0024685246 0.041490335 0.0015474006 5.051885e-05
#[5,] 0.0009112113 0.0004432334 0.011287550 0.0003030411 5.825822e-06
#[6,] 0.1772688245 0.1100027223 0.536616077 0.0711775639 7.689382e-03
#             [,6]
#[1,] 1.215408e-16
#[2,] 5.223774e-13
#[3,] 1.151519e-09
#[4,] 3.333130e-14
#[5,] 5.777122e-15
#[6,] 8.619454e-10



## primary VS late residual
pvalPriResL <- matrix(rep(0,36),ncol=6)
for(i in 1:6){
    for(j in 1:6){
        pvalPriResL[i,j] <- wilcox.test(pricDat[,i],pieResL[,j])$p.value
   }
}
pvalPriResL
## consider all the barcode in the library
#              [,1]          [,2]          [,3]          [,4]          [,5]
#[1,] 6.233669e-134 2.851065e-138 1.680324e-121 1.487060e-110 9.095314e-174
#[2,] 2.057735e-159 3.385738e-164 5.803204e-146 6.474016e-134 1.557443e-202
#[3,]  2.030312e-14  9.003474e-16  1.517896e-10  1.189760e-07  4.036505e-29
#[4,] 1.353117e-262 1.301045e-268 1.297382e-245 4.404364e-230  0.000000e+00
#[5,] 2.879864e-243 3.013880e-249 5.290315e-227 5.697925e-212 2.631096e-295
#[6,] 7.126317e-126 3.488109e-130 6.573487e-114 3.094561e-103 9.139727e-165
#              [,6]
#[1,] 4.333815e-137
#[2,] 5.192914e-163
#[3,]  2.649262e-15
#[4,] 1.837233e-267
#[5,] 5.405642e-248
#[6,] 4.510157e-129

## consider only the top 500 most abandunt barcodes
#             [,1]         [,2]         [,3]         [,4]          [,5]
#[1,] 1.977034e-85 1.237541e-78 2.190316e-67 2.051789e-72 3.083951e-118
#[2,] 5.901752e-73 9.980818e-68 2.181453e-57 4.077497e-61 1.850622e-104
#[3,] 4.515863e-45 4.995226e-40 4.658333e-33 9.504307e-36  1.396391e-67
#[4,] 1.012986e-73 2.191494e-68 6.474861e-58 3.385700e-61 1.208231e-107
#[5,] 5.862955e-76 2.668684e-71 5.718436e-61 4.209242e-64 6.062603e-110
#[6,] 1.547081e-57 8.688578e-54 1.635387e-44 1.133240e-46  1.107918e-89
#              [,6]
#[1,] 7.690777e-123
#[2,] 1.537878e-110
#[3,]  4.314659e-71
#[4,] 3.920921e-115
#[5,] 6.262969e-118
#[6,]  1.105704e-96

## consider only the top 100 most abundant barcodes
#             [,1]         [,2]         [,3]         [,4]         [,5]
#[1,] 3.805300e-13 1.004970e-15 5.128800e-10 4.248920e-10 9.476735e-24
#[2,] 1.074433e-09 3.006144e-12 1.401673e-07 2.218813e-07 7.578711e-19
#[3,] 3.293956e-07 6.378241e-09 3.079786e-05 2.407427e-05 1.437771e-14
#[4,] 2.127634e-10 3.508361e-13 2.807681e-08 6.689897e-08 3.245115e-20
#[5,] 1.231320e-11 3.108139e-14 4.248132e-09 4.386217e-09 6.608099e-22
#[6,] 8.794237e-07 4.653734e-09 3.703000e-05 7.440678e-05 5.716746e-15
#             [,6]
#[1,] 4.243170e-21
#[2,] 8.224945e-17
#[3,] 1.363634e-12
#[4,] 4.528983e-18
#[5,] 3.609080e-19
#[6,] 4.985573e-13


## early residual VS late residual
pvalResEL <- matrix(rep(0,36),ncol=6)
for(i in 1:6){
     for(j in 1:6){
        pvalResEL[i,j] <- wilcox.test(pieResE[,i], pieResL[,j])$p.value
     }
}
pvalResEL
## consider all the barcode in the library
#             [,1]         [,2]         [,3]         [,4]         [,5]
#[1,] 5.348506e-12 2.740074e-13 1.455816e-08 5.470987e-06 1.185757e-25
#[2,] 2.068908e-19 4.509169e-21 7.252546e-15 2.595101e-11 2.965934e-36
#[3,] 2.459688e-46 7.661726e-49 5.463870e-39 5.764125e-33 4.758278e-71
#[4,] 5.989457e-15 2.155639e-16 4.926342e-11 4.890610e-08 5.643635e-30
#[5,] 6.609051e-22 1.120824e-23 4.767709e-17 3.440961e-13 1.070415e-39
#[6,] 8.653354e-01 5.513035e-01 2.918500e-01 2.947677e-02 1.645512e-04
#             [,6]
#[1,] 7.684383e-13
#[2,] 1.876803e-20
#[3,] 5.651178e-48
#[4,] 7.152373e-16
#[5,] 4.847458e-23
#[6,] 6.663546e-01

## consider only the top 500 most abundant barcodes
#             [,1]         [,2]         [,3]         [,4]         [,5]
#[1,] 8.682888e-05 2.346353e-04 1.084862e-02 1.014203e-02 1.268108e-13
#[2,] 1.449427e-04 5.034491e-04 2.371553e-02 1.592282e-02 5.426695e-13
#[3,] 7.085381e-09 9.114947e-08 2.918190e-05 1.194103e-05 3.738303e-20
#[4,] 3.626634e-04 1.216767e-03 3.801386e-02 3.064695e-02 4.331895e-12
#[5,] 1.105611e-02 2.398250e-02 2.771624e-01 2.544875e-01 2.092348e-09
#[6,] 1.419694e-04 1.600433e-04 7.434599e-07 5.130503e-07 6.329270e-01
#             [,6]
#[1,] 3.886974e-17
#[2,] 1.067521e-13
#[3,] 9.996575e-23
#[4,] 3.826842e-14
#[5,] 6.495282e-11
#[6,] 6.970475e-01

## consider only the top 100 most abundant barcodes.
#             [,1]         [,2]         [,3]         [,4]         [,5]
#[1,] 1.419716e-03 2.688013e-05 0.0092199001 0.0152491714 1.112142e-09
#[2,] 2.807494e-03 6.179779e-05 0.0181506236 0.0276980594 4.610762e-09
#[3,] 2.911308e-05 1.742201e-07 0.0004142315 0.0008354725 2.533349e-12
#[4,] 7.711303e-03 2.018640e-04 0.0310319385 0.0569826119 3.083583e-08
#[5,] 5.398136e-02 1.494240e-03 0.1147316321 0.2354068819 8.409007e-07
#[6,] 8.038592e-02 7.948969e-01 0.0819383262 0.0229587915 1.805786e-01
#             [,6]
#[1,] 3.955087e-08
#[2,] 1.699163e-07
#[3,] 8.881474e-11
#[4,] 8.451290e-07
#[5,] 1.072151e-05
#[6,] 4.798743e-01



## figure 5D
# comparison of the Shannon index between Met-amplified and non-amplified tumors
t.test(sIndexRcurMet$NonAMP, sIndexRcurMet$AMP)
#    Welch Two Sample t-test
#
#data:  sIndexRcurMet$NonAMP and sIndexRcurMet$AMP
#t = 1.1626, df = 6.2135, p-value = 0.2877
#alternative hypothesis: true difference in means is not equal to 0
#95 percent confidence interval:
# -1.302074  3.697176
#sample estimates:
#mean of x mean of y 
# 2.760406  1.56285


library(xlsx)
RecurTime <- read.xlsx(file.path(wd, "./Data/recurrenceTime.xlsx"),
                       sheetName="Sheet1", stringsAsFactors=FALSE)
RecurTime <- RecurTime[,c(3,4)]
colnames(RecurTime) <- c("ID", "time")

cor.test(RecurTime$time, sIndex[RecurTime$ID,],method="spearman")

#    Spearman's rank correlation rho
#
#data:  RecurTime$time and sIndex[RecurTime$ID, ]
#S = 154.61, p-value = 0.133
#alternative hypothesis: true rho is not equal to 0
#sample estimates:
#      rho 
#0.4593898 
# there is not correlation between the recurrent time and shannon index

library(latex2exp)
pdf(file.path(wd, "Figures/ShannonIndexVSRecurrenceTime.pdf"), width=4,
    height=2.5)
par( mar=c(2,2,.5,.5),
     mgp=c(1.0, 0.1, 0),
     oma=c(0,0, 0, 0), tck=-0.015)
plot(RecurTime$time, sIndex[RecurTime$ID,], xlab="Time to recurrence (days)",
     ylab ="Shannon Index", col="blue", pch=16)
res <- lm(sIndex[RecurTime$ID,]~RecurTime$time)
abline(res)
textRsq <- TeX(paste0('$R^2=', substr(as.character(summary(res)$r.squared), 1, 5),'$'))
text(90,5.1, textRsq)#, col=colors[1])
dev.off()





















