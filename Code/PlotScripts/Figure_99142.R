library(beeswarm)
library(RColorBrewer)
library(reshape2)
suppressMessages(library(dplyr, warn.conflicts = FALSE, quietly=TRUE))
suppressMessages(library(plyr, warn.conflicts = FALSE, quietly=TRUE))
#library(tidyverse)
library(vegan)
library(ggplot2)
## slides 1
# data dir
#rm(list=ls())

wd    <- "/home/jl354@dhe.duke.edu/Work_old/Works/05_Alvarez/alvarez-barcode-paper-git/"
metaF <- "/Data/Alvarez_Cohort_2017.csv"
cF    <- "/Data/all-counts_2017.tsv"



# read in count data and meta info
cDat    <- read.table(file.path(wd, cF), header = TRUE, sep=",",
                    stringsAsFactors=FALSE, check.names=FALSE)
meta    <- read.csv(file.path(wd, metaF), header=FALSE, stringsAsFactors=FALSE)

sNames <- colnames(cDat)
for(i in 1:length(sNames))
      sNames[i] <- strsplit(sNames[i],"_")[[1]][1]
colnames(cDat) <- sNames
meta$V1 <- toupper(meta$V1)
colnames(meta) <- c("sNames", "Group")
rownames(cDat) <- cDat$barcode
cDat <- cDat[, meta$sNames]



## slides 3-1
uniBarNum <- data.frame(colSums(cDat > 1))

## slides 3
minCopies <- 1
ShannonIndex <- function(i, Dat, minCopies)
{  
    cDat <- Dat[,i]
    cDat <- cDat[which(cDat > minCopies)]
    cDat <- cDat/sum(cDat)
    -sum(cDat*log(cDat))
}
sIndex <- sapply(1:ncol(cDat), ShannonIndex, Dat=cDat, minCopies=minCopies)
sIndex <- data.frame(sIndex)
rownames(sIndex) <- meta$sNames
write.csv(sIndex, file="./sIndex_99142.csv")

PriInd      <- which(meta$Group=="Primary")
LibInd      <- which(meta$Group=="Library")
RCurInd     <- which(meta$Group=="Recurrent")

#cov_xy      <- cov(cDat)
#sd_x        <- sapply(cDat, sd, na.rm=TRUE)
#sd_x_time_x <- sd_x%*%t(sd_x)
#Pcorr       <- cov_xy/sd_x_time_x
## pearson correlation
cDatNorm    <- cDat
for(i in 1:ncol(cDat))
        cDatNorm[,i] <- cDat[,i]/sum(cDat[,i])
#cov_xy      <- cor(cDatNorm, method = c("spearman"))
#sd_x        <- sapply(cDat, sd, na.rm=TRUE)
#sd_x_time_x <- sd_x%*%t(sd_x)
#Pcorr       <- 1-abs(cov_xy)
JSD <- function(x,y)
{
    dat <- cbind(x,y)
    dat <- dat[which(x!=0 &y!=0),]
    sum(dat[,1]*log(dat[,1]/dat[,2])+ dat[,2]*log(dat[,2]/dat[,1]), na.rm=TRUE)/2
}
JS <- matrix(rep(0, ncol(cDatNorm)^2), ncol=ncol(cDatNorm))
for(i in 1:(ncol(cDatNorm)-1)){
   for(j in (i+1):ncol(cDatNorm)){
    JS[i,j] <- JSD(cDatNorm[,i], cDatNorm[,j])
    JS[j,i] <- JS[i,j]
    }
}
Pcorr       <- abs(JS)
source(file.path(wd, "Code/PlotScripts/HDCInteaction.R"))
Pcorr  <- JS


colorPlt   <- brewer.pal.info[brewer.pal.info$category == 'seq',]
colorPlt   <- brewer.pal.info
colorPie1   <- unlist(mapply(brewer.pal, colorPlt$maxcolors, rownames(colorPlt)))
colorPieFull <- rep(colorPie1, 1879)#4112)
colorPieFull <- data.frame(x=colorPieFull[1:nrow(cDat)],stringsAsFactors = FALSE)
colorPie     <- unlist(colorPieFull)
rownames(colorPieFull) <- rownames(cDat)


## Fig 99142 A-B (pie)
pieLib      <- cDat[, LibInd]
piePri      <- cDat[, PriInd]
pieRcur     <- cDat[, RCurInd]

## Fig 99142 D
uniBarLPR <- NULL
uniBarLPR$Lib  <- uniBarNum[LibInd,1]
uniBarLPR$Pri  <- uniBarNum[PriInd,1]
uniBarLPR$Rcur <- uniBarNum[RCurInd,1]

## Fig 99142 E
sIndexLPR <- NULL
sIndexLPR$Lib  <- sIndex[LibInd,1]
sIndexLPR$Pri  <- sIndex[PriInd,1]
sIndexLPR$Rcur <- sIndex[RCurInd,1]


## Fig 99142 F
sIndexLPR <- NULL
sIndexLPR$Lib  <- sIndex[LibInd,1]
sIndexLPR$Pri  <- sIndex[PriInd,1]
sIndexLPR$Rcur <- sIndex[RCurInd,1]




## Fig 99142 F
PcorPriRcur <-Pcorr[c(PriInd,RCurInd), c(PriInd, RCurInd)]
#PcorPriRcur[which(abs(PcorPriRcur-1.000) < 0.001)] <- NA
#PcorPriRcur.list <- as.list(as.data.frame(PcorPriRcur))

source(file.path(wd,"Code/PlotScripts//plotPara.R"))

FigRatio <- 0.85 # (0.82~0.9 all looking nice)
pdf(file.path(fdir, "./Fig_99142.pdf"), width=8*FigRatio, height=7.2*FigRatio)

FigLayoutMat <- c(rep(1,3), rep(2,3), rep(3,3),rep(4,3), rep(5,3), 
                  rep(18,15),
                  rep(6,3), rep(7,3), rep(8,3),rep(9,3), rep(19,3),
                  rep(10,3), rep(11,3), rep(12,3),rep(13,3), rep(20,3),
                  rep(17,15),
                  rep(14,5), rep(15,5), rep(16,5))
             #     rep(24,15),
             #     rep(17,7), rep(18,7), rep(19,1))
                                                           
# set margin
par(mfrow=c(6, 15),
    mar=c(2, 3.5, 2, 0.85)*FigRatio, 
    mgp=c(1.2, 0.1, 0)*FigRatio, 
    oma=c(0, 2, 1, 2)*FigRatio, tck=-0.015,xpd=TRUE)
layout(matrix(FigLayoutMat, 6, 15, byrow = TRUE), 
  	   widths=rep(1,15), heights=c(0.4, 0.05, 0.4, 0.4, 0.05, 0.6))

source(file.path(wd,"Code/PlotScripts//Fig_99142_A.R"))
source(file.path(wd,"Code/PlotScripts//Fig_99142_B.R"))
source(file.path(wd,"Code/PlotScripts//Fig_99142_CDE.R"))
#source(file.path(wd,"Code/PlotScripts//Fig_99142_FG.R"))
dev.off()


library(pheatmap)


rownames(PcorPriRcur) <- c(paste0("Pri ", 1:length(PriInd)),
                           paste0("Rec ",1:length(RCurInd)))
colnames(PcorPriRcur) <- c(paste0("Pri ", 1:length(PriInd)), 
                           paste0("Rec ",1:length(RCurInd)))


pheatmap(PcorPriRcur, main="Dissimilarity",
        filename = file.path(fdir, "/Fig_99142_F.pdf"), width = 5, height = 5, 
        color= colorRampPalette(brewer.pal(n = 7, name="RdYlBu"))(100))

pheatmap(PcorPriRcur, main="Dissimilarity", cluster_rows = FALSE, cluster_cols =
         FALSE,
        filename = file.path(fdir, "/Fig_99142_F_non_cluster.pdf"), width = 5,
        height = 5, 
        color= colorRampPalette(brewer.pal(n = 7, name="RdYlBu"))(100))


library(grid)

cDatRecur <- cDat[,RCurInd]
cDatRecurMet <- cDatRecur[,c(1,2,3,6)]
cDatRecurMetNorm <- sapply(1:4, function(x,y) y[,x]/sum(y[,x]), y = cDatRecurMet)
rownames(cDatRecurMetNorm) <- rownames(cDat) 
colnames(cDatRecurMetNorm) <- c("#1","#2","#3","#6")
topHitID <- unique(as.vector(sapply(1:4, function(x,y) rownames(y)[order(-y[,x])[1:5]], y= cDatRecurMetNorm)))
cDatRecurMetNorm <- cDatRecurMetNorm[topHitID, ]


FigRatio <- 0.5 # (0.82~0.9 all looking nice)
cDatRecurMetNorm.m <- melt(cDatRecurMetNorm)
p <- ggplot(cDatRecurMetNorm.m, aes(x=Var2, y=Var1, fill = value)) + 
     geom_tile(color = "white") + labs(x="Rcurrent tumor") +
     scale_fill_gradient2(low = "steelblue", high = "red", mid = "yellow", 
     midpoint = 0.5, limit = c(0,1), space = "Lab", name="Barcode Ratio") +
     theme_minimal() + 
     theme(
     axis.title.y = element_blank(),
     panel.grid.major = element_blank(),
     panel.border = element_blank(),
     panel.background = element_blank(),
     axis.ticks = element_blank())
pdf(file.path(fdir, "./Fig_99142_G.pdf"), width=10*FigRatio, height=6.5*FigRatio)
p
dev.off()














