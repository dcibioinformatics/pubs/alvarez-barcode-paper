wd <- "/home/jl354@dhe.duke.edu/Work_old/Works/05_Alvarez/alvarez-barcode-paper-git"
source(file.path(wd,"Code/PlotScripts/dataNeeded.R"))
source(file.path(wd,"Code/PlotScripts/plotPara.R"))

# Figure 5B heat map
RecurMetInd <- meta[meta$MET > 10 & meta$Group == "Rcur", "sNames"]
cDatRecurMet <- cDat[,RecurMetInd]
rownames(cDatRecurMet) <- rownames(cDat)

topHitID <- c("15952:1479", "6219:12837", "2724:16177","7444:4333", "5201:13058",
              "9171:13696", "6855:166", "896:5319", "16173:12735", "4435:6615", "13221:16221", "14655:8502",
              "260:13597", "13040:6803","4980:1034","7965:3189", "1117:9489","8546:16225")

cDatRecurMetNorm <- sapply(1:length(RecurMetInd), function(x,y) y[,x]/sum(y[,x]), y = cDatRecurMet)
rownames(cDatRecurMetNorm) <- rownames(cDatRecurMet) 
cDatRecurMetNorm <- cDatRecurMetNorm[topHitID, ]
colnames(cDatRecurMetNorm) <- paste0("#", c(4,5,6, 7,9,11))
## adding heat map plotting code here
breaksList = seq(0, 1, by = 0.01)
library(pheatmap)
library(grid)
cDatRecurMetNorm.m <- melt(cDatRecurMetNorm)
p <- ggplot(cDatRecurMetNorm.m, aes(x=Var2, y=Var1, fill = value)) + 
     geom_tile(color = "white") + labs(x="Recurrent tumor") +
     scale_fill_gradient2(low = "steelblue", high = "red", mid = "yellow", 
     midpoint = 0.5, limit = c(0,1), space = "Lab", name="Barcode Ratio") +
     theme_minimal() + 
     theme(
     axis.title.y = element_blank(),
     panel.grid.major = element_blank(),
     panel.border = element_blank(),
     panel.background = element_blank(),
     axis.ticks = element_blank())
FigRatio <- 0.5
pdf(file.path(fdir, "./Figure_5B.pdf"), width=10*FigRatio, height=6.5*FigRatio)
p
dev.off()


# Figure 5D 
library(xlsx)
RecurTime <- read.xlsx(file.path(wd, "./Data/recurrenceTime.xlsx"),
                       sheetName="Sheet1", stringsAsFactors=FALSE)
RecurTime <- RecurTime[,c(3,4)]
colnames(RecurTime) <- c("ID", "time")

library(latex2exp)
pdf(file.path(fdir, "Figure_5D.pdf"), width=4,
    height=2.5)
par( mar=c(2,2,.5,.5),
     mgp=c(1.0, 0.1, 0),
     oma=c(0,0, 0, 0), tck=-0.015)
plot(RecurTime$time, sIndex[RecurTime$ID,], xlab="Time to recurrence (days)",
     ylab ="Shannon Index", col="blue", pch=16)
res <- lm(sIndex[RecurTime$ID,]~RecurTime$time)
abline(res)
textRsq <- TeX(paste0('$R^2=', substr(as.character(summary(res)$r.squared), 1, 5),'$'))
text(90,5.1, textRsq)#, col=colors[1])
dev.off()



# Figure 5G 
FigRatio <- 0.5 # (0.82~0.9 all looking nice)
pdf(file.path(fdir, "./Figure_5G.pdf"), width=4*FigRatio, height=8*FigRatio)
par(mfrow=c(2, 1),
	mar=c(1, 1, 1, 1)*FigRatio,
    mgp=c(1.5, 0.3, 0)*FigRatio,
    oma=c(1, 2, 1, 2), tck=-0.015,xpd=NA)
for(i in 1:2){
  count <- pieNew[which(pieNew[,i]>minCopies),i]
  colorPietmp <- colorPie[which(pieNew[,i]>minCopies)]
  pie(count, labels=NA, col=colorPietmp, border = FALSE, radius=1.25,xpd=NA)
  text(0, -1.5, paste(i+1667))
}

dev.off()





