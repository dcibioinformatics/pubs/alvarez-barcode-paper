#include <Rcpp.h>
using namespace Rcpp;

// This is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar). Learn
// more about Rcpp at:
//
//   http://www.rcpp.org/
//   http://adv-r.had.co.nz/Rcpp.html
//   http://gallery.rcpp.org/
//

// [[Rcpp::export]]
NumericVector simReadCount(NumericVector sampledInt_sorted, NumericVector cumSplReadCnt) {
  int i = 0;
  int j = 0;
  NumericVector res;
  while(i<cumSplReadCnt.size() && j < sampledInt_sorted.size()){
    if(sampledInt_sorted[j] < cumSplReadCnt[i]){
      j++;
      continue;
    }
    if(sampledInt_sorted[j] >= cumSplReadCnt[i]){
      res.push_back(j);
      i++;
      j++;
      continue;
    }
  }
  return res;
}


// You can include R code blocks in C++ files processed with sourceCpp
// (useful for testing and development). The R code will be automatically 
// run after the compilation.
//

